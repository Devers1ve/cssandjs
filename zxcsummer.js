var curcontent = new Array();
//#region contents
/*curcontent["rules"] = {
    xhead: 'Правила',
    xcon: '<div class="xbox_custom_rules"><ul><p class="MsoNormal"><span>Основная информация</span></p>\
<p><li>Не знание правил не освобождает Вас от ответственности.</li></p>\
<p><li>Зайдя на сервер Вы автоматически соглашаетесь со всеми нижеперечисленными пунктами правил.</li></p>\
<p><li>Вы несете ответственность за все свои аккаунты. Получив бан на одном - Вы получите его и на других. То же самое будет, если на одном из ваших аккаунтах имеется EAC блокировка.</li></p>\
<p><li>Если Вы уже были замечены с читами / макросами на другом проекте и на вас есть доказательства - мы имеем право забанить Вас без проверки.</li></p>\
<p><li>Администрация не компенсирует игровые ценности, утраченные по причине вашей ошибки, серверных лагов, багов игры или контактом с нарушителями.</li></p>\
<p><li>Запрещена продажа или реклама Читов/Макросов.</li></p>\
<p><li>Запрещено выдавать себя за Администратора, модератора или проверяющего.</li></p>\
<p><li>Наказание для игрока зависит от степени нарушения и обстоятельств. Нарушитель может получить предупреждение, временный либо перманентный бан.</li></p>\
<p><li>При серьезном нарушении правил, блокировка выдаётся на всех серверах проекта. За игру сверх разрешенного лимита блокировка выдается только на сервере, где было зафиксировано нарушение.</li></p><br>\
<p class="MsoNormal"><span>Геймплей</span></li></p>\
<p><li>Запрещено использовать/хранить/распространять Читы/Макросы или любое другое ПО, позволяющие получить преимущество над другими игроками. Наличие покупки или подписки приравнивается к хранению стороннего ПО.</li></p>\
<p><li>Запрещена игра с читерами.</li></p>\
<p><li>Запрещено использование багов с целью или без цели получения преимущества над другими игроками.</li></p>\
<p><li>Запрещено использование услуг читеров.</li></p><br>\
<p class="MsoNormal"><span>Нарушение лимита игроков в команде</span></li></p>\
<p><li>Запрещено жить больше положенного лимита в одном доме.</li></p>\
<p><li>Запрещено устраивать альянсы и перемирия с соседями, если в сумме Вас больше, указанного в названии сервера, максимума.</li></p>\
<p><li>Чрезмерно частая смена тиммейтов будет считаться за нарушение.</li></p>\
<p><li>Запрещено рейдить/антирейдить или убивать в 1+/2+/3+ и т.д., в зависимости от максимально разрешенного количества игроков в команде, а так-же нельзя в целом каким-либо образом кооперироваться с игроками, если Ваша команда полная.</li></p><br>\
<p class="MsoNormal"><span>Игровой Чат</span></li></p>\
<p><li>Запрещены ссылки в чате на сторонние сервисы и сайты.</li></p>\
<p><li>Запрещен флуд (многократное повторение бессмысленных фраз, символов) или многократное отправление одинаковых фраз за короткий промежуток времени.</li></p>\
<p><li>Запрещено продавать или делать вид, что Вы продаёте Читы/Макросы.</li></p>\
<p><li>Запрещено продавать игровые ценности за реальную валюту (RMT).</li></p>\
<p><li>Модерация оставляет за собой право выдать мут игроку в чате, если тот нарушает правила чата.</li></p>\<br>\
<p class="MsoNormal"><span>Проверки</span></li></p>\
<p><li>Проверки проходят только через программу «Discord». Каждый игрок на нашем проекте, в обязательном порядке должен иметь эту программу на своём ПК.</li></p>\
<p><li>Вы имеете полное право отказаться проходить проверку, но в этом случае Вы будете заблокированы.</li></p>\
<p><li>Так же отказом от проверки будет считаться выход с сервера или предоставление некорректных контактных данных.</li></p>\
<p><li>Игнорирование вызова на проверку увенчается блокировкой.</li></p>\
<p><li>При согласии на проверку вы разрешаете устанавливать сторонние программы нужные администрации для проверки вашего ПК.</li></p>\
<p><li>Выход с сервера во время вызова на проверку закончится блокировкой.</li></p>\
<p><li>Запрещено чистить ПК перед проверкой.</li></p>\
<p><li>За отказ показывать нужную для проверки информацию или неадекватное поведение — Вы будете заблокированы.</li></p>\
<p><li>Если по итогам проверки игрок получает бан за читы — вся его команда блокируется вместе с ним.</li></p><br>\
<p class="MsoNormal"><span>Причины банов</span></li></p>\
<p><li>Использование читов - Бан навсегда, а также бан тиммейтов на 2 месяца.</li></p>\
<p><li>Использование услуг просвета - Бан навсегда.</li></p>\
<p><li>Отказ от проверки - Бан навсегда.</li></p>\
<p><li>Использование макросов - Бан на 1 год.</li></p>\
<p><li>Игра сверх разрешенного лимита - Бан на 1 месяц.</li></p><br>\
</ul><div class="unban" style="background: #333; border-radius: 10px; padding: 20px;"><p class="MsoNormal2" style="font-weight: bold; margin-bottom: 5px;">Возможность разблокировки</p><span>Не менее, чем через месяц после блокировки можно подать апелляцию, но только в том случае, если бан был получен не за читы или макросы. Писать апелляцию нужно в личные сообщения отдельной группы: <a href="https://vk.com/rusty_life_moder">vk.com/rusty_life_moder</a></span></div></div></div>'
};*/

curcontent["kit-prem"] = {
    xhead: 'Платные киты',
    xcon: '<div class="store-categories-1">\
	<button class="btn btn-secondary" onclick="Open(\'kit-free\', false);">Стандартные</button>\
	<button class="btn btn-secondary active">PREM</button>\
	<button class="btn btn-secondary" onclick="Open(\'kit-elite\', false);">ELITE</button>\
	<button class="btn btn-secondary" onclick="Open(\'kit-life\', false);">LIFE</button>\
	<button class="btn btn-secondary" onclick="Open(\'kit-rusty\', false);">RUSTY</button>\
	<div class="xbox_custom_kits" ><div class="serverheader">Кит "PVP" - <span>Откат 3ч</span></div>\
<div class="kit_set"><div class="kit-items">\
<div class="kit-items__item"><div class="kit-item"><img class="kit-item__image" src="https://static.moscow.ovh/images/games/rust/icons/smg.thompson.png">\
<div class="kit-item__quantity">x1</div></div></div>\
<div class="kit-items__item"><div class="kit-item"><img class="kit-item__image" src="https://static.moscow.ovh/images/games/rust/icons/ammo.pistol.png">\
<div class="kit-item__quantity">x200</div></div></div>\
<div class="kit-items__item"><div class="kit-item"><img class="kit-item__image" src="https://static.moscow.ovh/images/games/rust/icons/coffeecan.helmet.png">\
<div class="kit-item__quantity">x1</div></div></div>\
<div class="kit-items__item"><div class="kit-item"><img class="kit-item__image" src="https://static.moscow.ovh/images/games/rust/icons/roadsign.jacket.png">\
<div class="kit-item__quantity">x1</div></div></div>\
<div class="kit-items__item"><div class="kit-item"><img class="kit-item__image" src="https://static.moscow.ovh/images/games/rust/icons/roadsign.kilt.png">\
<div class="kit-item__quantity">x1</div></div></div>\
<div class="kit-items__item"><div class="kit-item"><img class="kit-item__image" src="https://static.moscow.ovh/images/games/rust/icons/tactical.gloves.png">\
<div class="kit-item__quantity">x1</div></div></div>\
<div class="kit-items__item"><div class="kit-item"><img class="kit-item__image" src="https://static.moscow.ovh/images/games/rust/icons/hoodie.png">\
<div class="kit-item__quantity">x1</div></div></div>\
<div class="kit-items__item"><div class="kit-item"><img class="kit-item__image" src="https://static.moscow.ovh/images/games/rust/icons/pants.png">\
<div class="kit-item__quantity">x1</div></div></div>\
<div class="kit-items__item"><div class="kit-item"><img class="kit-item__image" src="https://static.moscow.ovh/images/games/rust/icons/shoes.boots.png">\
<div class="kit-item__quantity">x1</div></div></div>\
<div class="kit-items__item"><div class="kit-item"><img class="kit-item__image" src="https://static.moscow.ovh/images/games/rust/icons/syringe.medical.png">\
<div class="kit-item__quantity">x10</div></div></div>\
<div class="kit-items__item"><div class="kit-item"><img class="kit-item__image" src="https://static.moscow.ovh/images/games/rust/icons/largemedkit.png">\
<div class="kit-item__quantity">x10</div></div></div>\
<div class="kit-items__item"><div class="kit-item"><img class="kit-item__image" src="https://static.moscow.ovh/images/games/rust/icons/blueberries.png">\
<div class="kit-item__quantity">x10</div></div></div>\
</div></div></div>\
<div class="xbox_custom_kits" ><div class="serverheader">Кит "Ресурсы" - <span>Откат 24ч</span></div>\
<div class="kit_set"><div class="kit-items">\
<div class="kit-items__item"><div class="kit-item"><img class="kit-item__image" src="https://static.moscow.ovh/images/games/rust/icons/wood.png">\
<div class="kit-item__quantity">x25000</div></div></div>\
<div class="kit-items__item"><div class="kit-item"><img class="kit-item__image" src="https://static.moscow.ovh/images/games/rust/icons/stones.png">\
<div class="kit-item__quantity">x25000</div></div></div>\
<div class="kit-items__item"><div class="kit-item"><img class="kit-item__image" src="https://static.moscow.ovh/images/games/rust/icons/metal.fragments.png">\
<div class="kit-item__quantity">x15000</div></div></div>\
<div class="kit-items__item"><div class="kit-item"><img class="kit-item__image" src="https://static.moscow.ovh/images/games/rust/icons/metal.refined.png">\
<div class="kit-item__quantity">x750</div></div></div>\
<div class="kit-items__item"><div class="kit-item"><img class="kit-item__image" src="https://static.moscow.ovh/images/games/rust/icons/cloth.png">\
<div class="kit-item__quantity">x2000</div></div></div>\
<div class="kit-items__item"><div class="kit-item"><img class="kit-item__image" src="https://static.moscow.ovh/images/games/rust/icons/leather.png">\
<div class="kit-item__quantity">x2000</div></div></div>\
<div class="kit-items__item"><div class="kit-item"><img class="kit-item__image" src="https://static.moscow.ovh/images/games/rust/icons/lowgradefuel.png">\
<div class="kit-item__quantity">x2000</div></div></div>\
<div class="kit-items__item"><div class="kit-item"><img class="kit-item__image" src="https://static.moscow.ovh/images/games/rust/icons/scrap.png">\
<div class="kit-item__quantity">x1000</div></div></div>\
<div class="kit-items__item"><div class="kit-item"><img class="kit-item__image" src="https://static.moscow.ovh/images/games/rust/icons/sulfur.png">\
<div class="kit-item__quantity">x15000</div></div></div>\
<div class="kit-items__item"><div class="kit-item"><img class="kit-item__image" src="https://static.moscow.ovh/images/games/rust/icons/charcoal.png">\
<div class="kit-item__quantity">x15000</div></div></div>\
<div class="kit-items__item"><div class="kit-item"><img class="kit-item__image" src="https://static.moscow.ovh/images/games/rust/icons/supply.signal.png">\
<div class="kit-item__quantity">x1</div></div></div>\
</div></div></div>\
<div class="xbox_custom_kits" ><div class="serverheader">Кит "Компоненты" - <span>Откат 24ч</span></div>\
<div class="kit_set"><div class="kit-items">\
<div class="kit-items__item"><div class="kit-item"><img class="kit-item__image" src="https://static.moscow.ovh/images/games/rust/icons/metalpipe.png">\
<div class="kit-item__quantity">x75</div></div></div>\
<div class="kit-items__item"><div class="kit-item"><img class="kit-item__image" src="https://static.moscow.ovh/images/games/rust/icons/techparts.png">\
<div class="kit-item__quantity">x25</div></div></div>\
<div class="kit-items__item"><div class="kit-item"><img class="kit-item__image" src="https://static.moscow.ovh/images/games/rust/icons/riflebody.png">\
<div class="kit-item__quantity">x5</div></div></div>\
<div class="kit-items__item"><div class="kit-item"><img class="kit-item__image" src="https://static.moscow.ovh/images/games/rust/icons/smgbody.png">\
<div class="kit-item__quantity">x5</div></div></div>\
<div class="kit-items__item"><div class="kit-item"><img class="kit-item__image" src="https://static.moscow.ovh/images/games/rust/icons/semibody.png">\
<div class="kit-item__quantity">x5</div></div></div>\
<div class="kit-items__item"><div class="kit-item"><img class="kit-item__image" src="https://static.moscow.ovh/images/games/rust/icons/metalspring.png">\
<div class="kit-item__quantity">x75</div></div></div>\
<div class="kit-items__item"><div class="kit-item"><img class="kit-item__image" src="https://static.moscow.ovh/images/games/rust/icons/sewingkit.png">\
<div class="kit-item__quantity">x75</div></div></div>\
<div class="kit-items__item"><div class="kit-item"><img class="kit-item__image" src="https://static.moscow.ovh/images/games/rust/icons/rope.png">\
<div class="kit-item__quantity">x75</div></div></div>\
<div class="kit-items__item"><div class="kit-item"><img class="kit-item__image" src="https://static.moscow.ovh/images/games/rust/icons/gears.png">\
<div class="kit-item__quantity">x75</div></div></div>\
<div class="kit-items__item"><div class="kit-item"><img class="kit-item__image" src="https://static.moscow.ovh/images/games/rust/icons/metalblade.png">\
<div class="kit-item__quantity">x75</div></div></div>\
<div class="kit-items__item"><div class="kit-item"><img class="kit-item__image" src="https://static.moscow.ovh/images/games/rust/icons/roadsigns.png">\
<div class="kit-item__quantity">x75</div></div></div>\
<div class="kit-items__item"><div class="kit-item"><img class="kit-item__image" src="https://static.moscow.ovh/images/games/rust/icons/tarp.png">\
<div class="kit-item__quantity">x50</div></div></div>\
</div></div></div>'
};

curcontent["kit-elite"] = {
    xhead: 'Платные киты',
    xcon: '<div class="store-categories-1">\
	<button class="btn btn-secondary" onclick="Open(\'kit-free\', false);">Стандартные</button>\
	<button class="btn btn-secondary" onclick="Open(\'kit-prem\', false);">PREM</button>\
	<button class="btn btn-secondary active">ELITE</button>\
	<button class="btn btn-secondary" onclick="Open(\'kit-life\', false);">LIFE</button>\
	<button class="btn btn-secondary" onclick="Open(\'kit-rusty\', false);">RUSTY</button>\
	<div class="xbox_custom_kits" ><div class="serverheader">Кит "PVP" - <span>Откат 3ч</span></div>\
<div class="kit_set"><div class="kit-items">\
<div class="kit-items__item"><div class="kit-item"><img class="kit-item__image" src="https://static.moscow.ovh/images/games/rust/icons/smg.mp5.png">\
<div class="kit-item__quantity">x1</div></div></div>\
<div class="kit-items__item"><div class="kit-item"><img class="kit-item__image" src="https://static.moscow.ovh/images/games/rust/icons/ammo.pistol.png">\
<div class="kit-item__quantity">x200</div></div></div>\
<div class="kit-items__item"><div class="kit-item"><img class="kit-item__image" src="https://static.moscow.ovh/images/games/rust/icons/rifle.semiauto.png">\
<div class="kit-item__quantity">x1</div></div></div>\
<div class="kit-items__item"><div class="kit-item"><img class="kit-item__image" src="https://static.moscow.ovh/images/games/rust/icons/ammo.rifle.png">\
<div class="kit-item__quantity">x200</div></div></div>\
<div class="kit-items__item"><div class="kit-item"><img class="kit-item__image" src="https://static.moscow.ovh/images/games/rust/icons/metal.facemask.png">\
<div class="kit-item__quantity">x1</div></div></div>\
<div class="kit-items__item"><div class="kit-item"><img class="kit-item__image" src="https://static.moscow.ovh/images/games/rust/icons/metal.plate.torso.png">\
<div class="kit-item__quantity">x1</div></div></div>\
<div class="kit-items__item"><div class="kit-item"><img class="kit-item__image" src="https://static.moscow.ovh/images/games/rust/icons/roadsign.kilt.png">\
<div class="kit-item__quantity">x1</div></div></div>\
<div class="kit-items__item"><div class="kit-item"><img class="kit-item__image" src="https://static.moscow.ovh/images/games/rust/icons/tactical.gloves.png">\
<div class="kit-item__quantity">x1</div></div></div>\
<div class="kit-items__item"><div class="kit-item"><img class="kit-item__image" src="https://static.moscow.ovh/images/games/rust/icons/hoodie.png">\
<div class="kit-item__quantity">x1</div></div></div>\
<div class="kit-items__item"><div class="kit-item"><img class="kit-item__image" src="https://static.moscow.ovh/images/games/rust/icons/pants.png">\
<div class="kit-item__quantity">x1</div></div></div>\
<div class="kit-items__item"><div class="kit-item"><img class="kit-item__image" src="https://static.moscow.ovh/images/games/rust/icons/shoes.boots.png">\
<div class="kit-item__quantity">x1</div></div></div>\
<div class="kit-items__item"><div class="kit-item"><img class="kit-item__image" src="https://static.moscow.ovh/images/games/rust/icons/syringe.medical.png">\
<div class="kit-item__quantity">x15</div></div></div>\
<div class="kit-items__item"><div class="kit-item"><img class="kit-item__image" src="https://static.moscow.ovh/images/games/rust/icons/largemedkit.png">\
<div class="kit-item__quantity">x15</div></div></div>\
<div class="kit-items__item"><div class="kit-item"><img class="kit-item__image" src="https://static.moscow.ovh/images/games/rust/icons/blueberries.png">\
<div class="kit-item__quantity">x25</div></div></div>\
</div></div></div>\
<div class="xbox_custom_kits" ><div class="serverheader">Кит "Ресурсы" - <span>Откат 24ч</span></div>\
<div class="kit_set"><div class="kit-items">\
<div class="kit-items__item"><div class="kit-item"><img class="kit-item__image" src="https://static.moscow.ovh/images/games/rust/icons/wood.png">\
<div class="kit-item__quantity">x50000</div></div></div>\
<div class="kit-items__item"><div class="kit-item"><img class="kit-item__image" src="https://static.moscow.ovh/images/games/rust/icons/stones.png">\
<div class="kit-item__quantity">x50000</div></div></div>\
<div class="kit-items__item"><div class="kit-item"><img class="kit-item__image" src="https://static.moscow.ovh/images/games/rust/icons/metal.fragments.png">\
<div class="kit-item__quantity">x30000</div></div></div>\
<div class="kit-items__item"><div class="kit-item"><img class="kit-item__image" src="https://static.moscow.ovh/images/games/rust/icons/metal.refined.png">\
<div class="kit-item__quantity">x1500</div></div></div>\
<div class="kit-items__item"><div class="kit-item"><img class="kit-item__image" src="https://static.moscow.ovh/images/games/rust/icons/cloth.png">\
<div class="kit-item__quantity">x4000</div></div></div>\
<div class="kit-items__item"><div class="kit-item"><img class="kit-item__image" src="https://static.moscow.ovh/images/games/rust/icons/leather.png">\
<div class="kit-item__quantity">x4000</div></div></div>\
<div class="kit-items__item"><div class="kit-item"><img class="kit-item__image" src="https://static.moscow.ovh/images/games/rust/icons/lowgradefuel.png">\
<div class="kit-item__quantity">x4000</div></div></div>\
<div class="kit-items__item"><div class="kit-item"><img class="kit-item__image" src="https://static.moscow.ovh/images/games/rust/icons/scrap.png">\
<div class="kit-item__quantity">x2000</div></div></div>\
<div class="kit-items__item"><div class="kit-item"><img class="kit-item__image" src="https://static.moscow.ovh/images/games/rust/icons/sulfur.png">\
<div class="kit-item__quantity">x30000</div></div></div>\
<div class="kit-items__item"><div class="kit-item"><img class="kit-item__image" src="https://static.moscow.ovh/images/games/rust/icons/charcoal.png">\
<div class="kit-item__quantity">x30000</div></div></div>\
<div class="kit-items__item"><div class="kit-item"><img class="kit-item__image" src="https://static.moscow.ovh/images/games/rust/icons/supply.signal.png">\
<div class="kit-item__quantity">x2</div></div></div>\
</div></div></div>\
<div class="xbox_custom_kits" ><div class="serverheader">Кит "Компоненты" - <span>Откат 24ч</span></div>\
<div class="kit_set"><div class="kit-items">\
<div class="kit-items__item"><div class="kit-item"><img class="kit-item__image" src="https://static.moscow.ovh/images/games/rust/icons/metalpipe.png">\
<div class="kit-item__quantity">x125</div></div></div>\
<div class="kit-items__item"><div class="kit-item"><img class="kit-item__image" src="https://static.moscow.ovh/images/games/rust/icons/techparts.png">\
<div class="kit-item__quantity">x50</div></div></div>\
<div class="kit-items__item"><div class="kit-item"><img class="kit-item__image" src="https://static.moscow.ovh/images/games/rust/icons/riflebody.png">\
<div class="kit-item__quantity">x15</div></div></div>\
<div class="kit-items__item"><div class="kit-item"><img class="kit-item__image" src="https://static.moscow.ovh/images/games/rust/icons/smgbody.png">\
<div class="kit-item__quantity">x15</div></div></div>\
<div class="kit-items__item"><div class="kit-item"><img class="kit-item__image" src="https://static.moscow.ovh/images/games/rust/icons/semibody.png">\
<div class="kit-item__quantity">x15</div></div></div>\
<div class="kit-items__item"><div class="kit-item"><img class="kit-item__image" src="https://static.moscow.ovh/images/games/rust/icons/metalspring.png">\
<div class="kit-item__quantity">x125</div></div></div>\
<div class="kit-items__item"><div class="kit-item"><img class="kit-item__image" src="https://static.moscow.ovh/images/games/rust/icons/sewingkit.png">\
<div class="kit-item__quantity">x125</div></div></div>\
<div class="kit-items__item"><div class="kit-item"><img class="kit-item__image" src="https://static.moscow.ovh/images/games/rust/icons/rope.png">\
<div class="kit-item__quantity">x125</div></div></div>\
<div class="kit-items__item"><div class="kit-item"><img class="kit-item__image" src="https://static.moscow.ovh/images/games/rust/icons/gears.png">\
<div class="kit-item__quantity">x125</div></div></div>\
<div class="kit-items__item"><div class="kit-item"><img class="kit-item__image" src="https://static.moscow.ovh/images/games/rust/icons/metalblade.png">\
<div class="kit-item__quantity">x125</div></div></div>\
<div class="kit-items__item"><div class="kit-item"><img class="kit-item__image" src="https://static.moscow.ovh/images/games/rust/icons/roadsigns.png">\
<div class="kit-item__quantity">x125</div></div></div>\
<div class="kit-items__item"><div class="kit-item"><img class="kit-item__image" src="https://static.moscow.ovh/images/games/rust/icons/tarp.png">\
<div class="kit-item__quantity">x75</div></div></div>\
</div></div></div>'
};

curcontent["kit-life"] = {
    xhead: 'Платные киты',
    xcon: '<div class="store-categories-1">\
	<button class="btn btn-secondary" onclick="Open(\'kit-free\', false);">Стандартные</button>\
	<button class="btn btn-secondary" onclick="Open(\'kit-prem\', false);">PREM</button>\
	<button class="btn btn-secondary" onclick="Open(\'kit-elite\', false);">ELITE</button>\
	<button class="btn btn-secondary active">LIFE</button>\
	<button class="btn btn-secondary" onclick="Open(\'kit-rusty\', false);">RUSTY</button>\
	<div class="xbox_custom_kits" ><div class="serverheader">Кит "PVP" - <span>Откат 3ч</span></div>\
<div class="kit_set"><div class="kit-items">\
<div class="kit-items__item"><div class="kit-item"><img class="kit-item__image" src="https://static.moscow.ovh/images/games/rust/icons/rifle.ak.png">\
<div class="kit-item__quantity">x1</div></div></div>\
<div class="kit-items__item"><div class="kit-item"><img class="kit-item__image" src="https://static.moscow.ovh/images/games/rust/icons/rifle.bolt.png">\
<div class="kit-item__quantity">x1</div></div></div>\
<div class="kit-items__item"><div class="kit-item"><img class="kit-item__image" src="https://static.moscow.ovh/images/games/rust/icons/ammo.rifle.png">\
<div class="kit-item__quantity">x300</div></div></div>\
<div class="kit-items__item"><div class="kit-item"><img class="kit-item__image" src="https://static.moscow.ovh/images/games/rust/icons/metal.facemask.png">\
<div class="kit-item__quantity">x1</div></div></div>\
<div class="kit-items__item"><div class="kit-item"><img class="kit-item__image" src="https://static.moscow.ovh/images/games/rust/icons/metal.plate.torso.png">\
<div class="kit-item__quantity">x1</div></div></div>\
<div class="kit-items__item"><div class="kit-item"><img class="kit-item__image" src="https://static.moscow.ovh/images/games/rust/icons/roadsign.kilt.png">\
<div class="kit-item__quantity">x1</div></div></div>\
<div class="kit-items__item"><div class="kit-item"><img class="kit-item__image" src="https://static.moscow.ovh/images/games/rust/icons/tactical.gloves.png">\
<div class="kit-item__quantity">x1</div></div></div>\
<div class="kit-items__item"><div class="kit-item"><img class="kit-item__image" src="https://static.moscow.ovh/images/games/rust/icons/hoodie.png">\
<div class="kit-item__quantity">x1</div></div></div>\
<div class="kit-items__item"><div class="kit-item"><img class="kit-item__image" src="https://static.moscow.ovh/images/games/rust/icons/pants.png">\
<div class="kit-item__quantity">x1</div></div></div>\
<div class="kit-items__item"><div class="kit-item"><img class="kit-item__image" src="https://static.moscow.ovh/images/games/rust/icons/shoes.boots.png">\
<div class="kit-item__quantity">x1</div></div></div>\
<div class="kit-items__item"><div class="kit-item"><img class="kit-item__image" src="https://static.moscow.ovh/images/games/rust/icons/syringe.medical.png">\
<div class="kit-item__quantity">x20</div></div></div>\
<div class="kit-items__item"><div class="kit-item"><img class="kit-item__image" src="https://static.moscow.ovh/images/games/rust/icons/largemedkit.png">\
<div class="kit-item__quantity">x20</div></div></div>\
<div class="kit-items__item"><div class="kit-item"><img class="kit-item__image" src="https://static.moscow.ovh/images/games/rust/icons/blueberries.png">\
<div class="kit-item__quantity">x40</div></div></div>\
</div></div></div>\
<div class="xbox_custom_kits" ><div class="serverheader">Кит "Ресурсы" - <span>Откат 24ч</span></div>\
<div class="kit_set"><div class="kit-items">\
<div class="kit-items__item"><div class="kit-item"><img class="kit-item__image" src="https://static.moscow.ovh/images/games/rust/icons/wood.png">\
<div class="kit-item__quantity">x75000</div></div></div>\
<div class="kit-items__item"><div class="kit-item"><img class="kit-item__image" src="https://static.moscow.ovh/images/games/rust/icons/stones.png">\
<div class="kit-item__quantity">x75000</div></div></div>\
<div class="kit-items__item"><div class="kit-item"><img class="kit-item__image" src="https://static.moscow.ovh/images/games/rust/icons/metal.fragments.png">\
<div class="kit-item__quantity">x50000</div></div></div>\
<div class="kit-items__item"><div class="kit-item"><img class="kit-item__image" src="https://static.moscow.ovh/images/games/rust/icons/metal.refined.png">\
<div class="kit-item__quantity">x2500</div></div></div>\
<div class="kit-items__item"><div class="kit-item"><img class="kit-item__image" src="https://static.moscow.ovh/images/games/rust/icons/cloth.png">\
<div class="kit-item__quantity">x5000</div></div></div>\
<div class="kit-items__item"><div class="kit-item"><img class="kit-item__image" src="https://static.moscow.ovh/images/games/rust/icons/leather.png">\
<div class="kit-item__quantity">x5000</div></div></div>\
<div class="kit-items__item"><div class="kit-item"><img class="kit-item__image" src="https://static.moscow.ovh/images/games/rust/icons/lowgradefuel.png">\
<div class="kit-item__quantity">x5000</div></div></div>\
<div class="kit-items__item"><div class="kit-item"><img class="kit-item__image" src="https://static.moscow.ovh/images/games/rust/icons/scrap.png">\
<div class="kit-item__quantity">x3000</div></div></div>\
<div class="kit-items__item"><div class="kit-item"><img class="kit-item__image" src="https://static.moscow.ovh/images/games/rust/icons/sulfur.png">\
<div class="kit-item__quantity">x50000</div></div></div>\
<div class="kit-items__item"><div class="kit-item"><img class="kit-item__image" src="https://static.moscow.ovh/images/games/rust/icons/charcoal.png">\
<div class="kit-item__quantity">x50000</div></div></div>\
<div class="kit-items__item"><div class="kit-item"><img class="kit-item__image" src="https://static.moscow.ovh/images/games/rust/icons/supply.signal.png">\
<div class="kit-item__quantity">x3</div></div></div>\
</div></div></div>\
<div class="xbox_custom_kits" ><div class="serverheader">Кит "Компоненты" - <span>Откат 24ч</span></div>\
<div class="kit_set"><div class="kit-items">\
<div class="kit-items__item"><div class="kit-item"><img class="kit-item__image" src="https://static.moscow.ovh/images/games/rust/icons/metalpipe.png">\
<div class="kit-item__quantity">x200</div></div></div>\
<div class="kit-items__item"><div class="kit-item"><img class="kit-item__image" src="https://static.moscow.ovh/images/games/rust/icons/techparts.png">\
<div class="kit-item__quantity">x100</div></div></div>\
<div class="kit-items__item"><div class="kit-item"><img class="kit-item__image" src="https://static.moscow.ovh/images/games/rust/icons/riflebody.png">\
<div class="kit-item__quantity">x30</div></div></div>\
<div class="kit-items__item"><div class="kit-item"><img class="kit-item__image" src="https://static.moscow.ovh/images/games/rust/icons/smgbody.png">\
<div class="kit-item__quantity">x30</div></div></div>\
<div class="kit-items__item"><div class="kit-item"><img class="kit-item__image" src="https://static.moscow.ovh/images/games/rust/icons/semibody.png">\
<div class="kit-item__quantity">x30</div></div></div>\
<div class="kit-items__item"><div class="kit-item"><img class="kit-item__image" src="https://static.moscow.ovh/images/games/rust/icons/metalspring.png">\
<div class="kit-item__quantity">x200</div></div></div>\
<div class="kit-items__item"><div class="kit-item"><img class="kit-item__image" src="https://static.moscow.ovh/images/games/rust/icons/sewingkit.png">\
<div class="kit-item__quantity">x200</div></div></div>\
<div class="kit-items__item"><div class="kit-item"><img class="kit-item__image" src="https://static.moscow.ovh/images/games/rust/icons/rope.png">\
<div class="kit-item__quantity">x200</div></div></div>\
<div class="kit-items__item"><div class="kit-item"><img class="kit-item__image" src="https://static.moscow.ovh/images/games/rust/icons/gears.png">\
<div class="kit-item__quantity">x200</div></div></div>\
<div class="kit-items__item"><div class="kit-item"><img class="kit-item__image" src="https://static.moscow.ovh/images/games/rust/icons/metalblade.png">\
<div class="kit-item__quantity">x200</div></div></div>\
<div class="kit-items__item"><div class="kit-item"><img class="kit-item__image" src="https://static.moscow.ovh/images/games/rust/icons/roadsigns.png">\
<div class="kit-item__quantity">x200</div></div></div>\
<div class="kit-items__item"><div class="kit-item"><img class="kit-item__image" src="https://static.moscow.ovh/images/games/rust/icons/tarp.png">\
<div class="kit-item__quantity">x100</div></div></div>\
</div></div></div>'
};

curcontent["kit-rusty"] = {
    xhead: 'Платные киты',
    xcon: '<div class="store-categories-1">\
	<button class="btn btn-secondary" onclick="Open(\'kit-free\', false);">Стандартные</button>\
	<button class="btn btn-secondary" onclick="Open(\'kit-prem\', false);">PREM</button>\
	<button class="btn btn-secondary" onclick="Open(\'kit-elite\', false);">ELITE</button>\
	<button class="btn btn-secondary" onclick="Open(\'kit-life\', false);">LIFE</button>\
	<button class="btn btn-secondary active">RUSTY</button>\
	<div class="xbox_custom_kits" ><div class="serverheader">Кит "PVP" - <span>Откат 3ч</span></div>\
<div class="kit_set"><div class="kit-items">\
<div class="kit-items__item"><div class="kit-item"><img class="kit-item__image" src="https://static.moscow.ovh/images/games/rust/icons/rifle.ak.png">\
<div class="kit-item__quantity">x1</div></div></div>\
<div class="kit-items__item"><div class="kit-item"><img class="kit-item__image" src="https://www.rustedit.io/images/imagelibrary/hmlmg.png">\
<div class="kit-item__quantity">x1</div></div></div>\
<div class="kit-items__item"><div class="kit-item"><img class="kit-item__image" src="https://static.moscow.ovh/images/games/rust/icons/rifle.bolt.png">\
<div class="kit-item__quantity">x1</div></div></div>\
<div class="kit-items__item"><div class="kit-item"><img class="kit-item__image" src="https://static.moscow.ovh/images/games/rust/icons/ammo.rifle.png">\
<div class="kit-item__quantity">x500</div></div></div>\
<div class="kit-items__item"><div class="kit-item"><img class="kit-item__image" src="https://static.moscow.ovh/images/games/rust/icons/metal.facemask.png">\
<div class="kit-item__quantity">x1</div></div></div>\
<div class="kit-items__item"><div class="kit-item"><img class="kit-item__image" src="https://static.moscow.ovh/images/games/rust/icons/metal.plate.torso.png">\
<div class="kit-item__quantity">x1</div></div></div>\
<div class="kit-items__item"><div class="kit-item"><img class="kit-item__image" src="https://static.moscow.ovh/images/games/rust/icons/roadsign.kilt.png">\
<div class="kit-item__quantity">x1</div></div></div>\
<div class="kit-items__item"><div class="kit-item"><img class="kit-item__image" src="https://static.moscow.ovh/images/games/rust/icons/tactical.gloves.png">\
<div class="kit-item__quantity">x1</div></div></div>\
<div class="kit-items__item"><div class="kit-item"><img class="kit-item__image" src="https://static.moscow.ovh/images/games/rust/icons/hoodie.png">\
<div class="kit-item__quantity">x1</div></div></div>\
<div class="kit-items__item"><div class="kit-item"><img class="kit-item__image" src="https://static.moscow.ovh/images/games/rust/icons/pants.png">\
<div class="kit-item__quantity">x1</div></div></div>\
<div class="kit-items__item"><div class="kit-item"><img class="kit-item__image" src="https://static.moscow.ovh/images/games/rust/icons/shoes.boots.png">\
<div class="kit-item__quantity">x1</div></div></div>\
<div class="kit-items__item"><div class="kit-item"><img class="kit-item__image" src="https://static.moscow.ovh/images/games/rust/icons/syringe.medical.png">\
<div class="kit-item__quantity">x20</div></div></div>\
<div class="kit-items__item"><div class="kit-item"><img class="kit-item__image" src="https://static.moscow.ovh/images/games/rust/icons/largemedkit.png">\
<div class="kit-item__quantity">x20</div></div></div>\
<div class="kit-items__item"><div class="kit-item"><img class="kit-item__image" src="https://static.moscow.ovh/images/games/rust/icons/blueberries.png">\
<div class="kit-item__quantity">x50</div></div></div>\
</div></div></div>\
<div class="xbox_custom_kits" ><div class="serverheader">Кит "Ресурсы" - <span>Откат 24ч</span></div>\
<div class="kit_set"><div class="kit-items">\
<div class="kit-items__item"><div class="kit-item"><img class="kit-item__image" src="https://static.moscow.ovh/images/games/rust/icons/wood.png">\
<div class="kit-item__quantity">x100000</div></div></div>\
<div class="kit-items__item"><div class="kit-item"><img class="kit-item__image" src="https://static.moscow.ovh/images/games/rust/icons/stones.png">\
<div class="kit-item__quantity">x100000</div></div></div>\
<div class="kit-items__item"><div class="kit-item"><img class="kit-item__image" src="https://static.moscow.ovh/images/games/rust/icons/metal.fragments.png">\
<div class="kit-item__quantity">x75000</div></div></div>\
<div class="kit-items__item"><div class="kit-item"><img class="kit-item__image" src="https://static.moscow.ovh/images/games/rust/icons/metal.refined.png">\
<div class="kit-item__quantity">x4000</div></div></div>\
<div class="kit-items__item"><div class="kit-item"><img class="kit-item__image" src="https://static.moscow.ovh/images/games/rust/icons/cloth.png">\
<div class="kit-item__quantity">x7500</div></div></div>\
<div class="kit-items__item"><div class="kit-item"><img class="kit-item__image" src="https://static.moscow.ovh/images/games/rust/icons/leather.png">\
<div class="kit-item__quantity">x7500</div></div></div>\
<div class="kit-items__item"><div class="kit-item"><img class="kit-item__image" src="https://static.moscow.ovh/images/games/rust/icons/lowgradefuel.png">\
<div class="kit-item__quantity">x7500</div></div></div>\
<div class="kit-items__item"><div class="kit-item"><img class="kit-item__image" src="https://static.moscow.ovh/images/games/rust/icons/scrap.png">\
<div class="kit-item__quantity">x4000</div></div></div>\
<div class="kit-items__item"><div class="kit-item"><img class="kit-item__image" src="https://static.moscow.ovh/images/games/rust/icons/sulfur.png">\
<div class="kit-item__quantity">x65000</div></div></div>\
<div class="kit-items__item"><div class="kit-item"><img class="kit-item__image" src="https://static.moscow.ovh/images/games/rust/icons/charcoal.png">\
<div class="kit-item__quantity">x65000</div></div></div>\
<div class="kit-items__item"><div class="kit-item"><img class="kit-item__image" src="https://static.moscow.ovh/images/games/rust/icons/supply.signal.png">\
<div class="kit-item__quantity">x4</div></div></div>\
</div></div></div>\
<div class="xbox_custom_kits" ><div class="serverheader">Кит "Компоненты" - <span>Откат 24ч</span></div>\
<div class="kit_set"><div class="kit-items">\
<div class="kit-items__item"><div class="kit-item"><img class="kit-item__image" src="https://static.moscow.ovh/images/games/rust/icons/metalpipe.png">\
<div class="kit-item__quantity">x250</div></div></div>\
<div class="kit-items__item"><div class="kit-item"><img class="kit-item__image" src="https://static.moscow.ovh/images/games/rust/icons/techparts.png">\
<div class="kit-item__quantity">x150</div></div></div>\
<div class="kit-items__item"><div class="kit-item"><img class="kit-item__image" src="https://static.moscow.ovh/images/games/rust/icons/riflebody.png">\
<div class="kit-item__quantity">x50</div></div></div>\
<div class="kit-items__item"><div class="kit-item"><img class="kit-item__image" src="https://static.moscow.ovh/images/games/rust/icons/smgbody.png">\
<div class="kit-item__quantity">x50</div></div></div>\
<div class="kit-items__item"><div class="kit-item"><img class="kit-item__image" src="https://static.moscow.ovh/images/games/rust/icons/semibody.png">\
<div class="kit-item__quantity">x50</div></div></div>\
<div class="kit-items__item"><div class="kit-item"><img class="kit-item__image" src="https://static.moscow.ovh/images/games/rust/icons/metalspring.png">\
<div class="kit-item__quantity">x250</div></div></div>\
<div class="kit-items__item"><div class="kit-item"><img class="kit-item__image" src="https://static.moscow.ovh/images/games/rust/icons/sewingkit.png">\
<div class="kit-item__quantity">x250</div></div></div>\
<div class="kit-items__item"><div class="kit-item"><img class="kit-item__image" src="https://static.moscow.ovh/images/games/rust/icons/rope.png">\
<div class="kit-item__quantity">x250</div></div></div>\
<div class="kit-items__item"><div class="kit-item"><img class="kit-item__image" src="https://static.moscow.ovh/images/games/rust/icons/gears.png">\
<div class="kit-item__quantity">x250</div></div></div>\
<div class="kit-items__item"><div class="kit-item"><img class="kit-item__image" src="https://static.moscow.ovh/images/games/rust/icons/metalblade.png">\
<div class="kit-item__quantity">x250</div></div></div>\
<div class="kit-items__item"><div class="kit-item"><img class="kit-item__image" src="https://static.moscow.ovh/images/games/rust/icons/roadsigns.png">\
<div class="kit-item__quantity">x250</div></div></div>\
<div class="kit-items__item"><div class="kit-item"><img class="kit-item__image" src="https://static.moscow.ovh/images/games/rust/icons/tarp.png">\
<div class="kit-item__quantity">x200</div></div></div>\
</div></div>\
<div class="xbox_custom_kits" ><div class="serverheader">Кит "Дополнительный" - <span>1 Раз в вайп</span></div>\
<div class="kit_set"><div class="kit-items">\
<div class="kit-items__item"><div class="kit-item"><img class="kit-item__image" src="https://static.moscow.ovh/images/games/rust/icons/workbench3.png">\
<div class="kit-item__quantity">x1</div></div></div>\
<div class="kit-items__item"><div class="kit-item"><img class="kit-item__image" src="https://static.moscow.ovh/images/games/rust/icons/workbench2.png">\
<div class="kit-item__quantity">x1</div></div></div>\
<div class="kit-items__item"><div class="kit-item"><img class="kit-item__image" src="https://static.moscow.ovh/images/games/rust/icons/workbench1.png">\
<div class="kit-item__quantity">x1</div></div></div>\
<div class="kit-items__item"><div class="kit-item"><img class="kit-item__image" src="https://static.moscow.ovh/images/games/rust/icons/mining.quarry.png">\
<div class="kit-item__quantity">x1</div></div></div>\
<div class="kit-items__item"><div class="kit-item"><img class="kit-item__image" src="https://static.moscow.ovh/images/games/rust/icons/surveycharge.png">\
<div class="kit-item__quantity">x50</div></div></div>\
<div class="kit-items__item"><div class="kit-item"><img class="kit-item__image" src="https://static.moscow.ovh/images/games/rust/icons/autoturret.png">\
<div class="kit-item__quantity">x5</div></div></div>\
<div class="kit-items__item"><div class="kit-item"><img class="kit-item__image" src="https://static.moscow.ovh/images/games/rust/icons/coffin.storage.png">\
<div class="kit-item__quantity">x10</div></div></div>\
<div class="kit-items__item"><div class="kit-item"><img class="kit-item__image" src="https://pic.moscow.ovh/images/2022/07/23/ea48412ea9e6c5c530dca8a147c413e5.png">\
<div class="kit-item__quantity">x1</div></div></div>\
<div class="kit-items__item"><div class="kit-item"><img class="kit-item__image" src="https://pic.moscow.ovh/images/2022/07/23/18792a460ae3f80eff76f080043173e6.png">\
<div class="kit-item__quantity">x1</div></div></div>\
<div class="kit-items__item"><div class="kit-item"><img class="kit-item__image" src="https://static.moscow.ovh/images/games/rust/icons/black.raspberries.png">\
<div class="kit-item__quantity">x50</div></div></div>\
<div class="kit-items__item"><div class="kit-item"><img class="kit-item__image" src="https://static.moscow.ovh/images/games/rust/icons/blueberries.png">\
<div class="kit-item__quantity">x50</div></div></div>\
</div></div></div>'
};

curcontent["kit-free"] = {
    xhead: 'Стандартные киты',
    xcon: '<div class="store-categories-1">\
	<button class="btn btn-secondary active">Стандартные</button>\
	<button class="btn btn-secondary" onclick="Open(\'kit-prem\', false);">PREM</button>\
	<button class="btn btn-secondary" onclick="Open(\'kit-elite\', false);">ELITE</button>\
	<button class="btn btn-secondary" onclick="Open(\'kit-life\', false);">LIFE</button>\
	<button class="btn btn-secondary" onclick="Open(\'kit-rusty\', false);">RUSTY</button>\
<div class="xbox_custom_kits" >\
<div class="serverheader">Кит "Дом" - <span>Откат 12ч</span></div>\
<div class="kit_set"><div class="kit-items">\
<div class="kit-items__item"><div class="kit-item"><img class="kit-item__image" src="https://static.moscow.ovh/images/games/rust/icons/wood.png">\
<div class="kit-item__quantity">x5000</div></div></div>\
<div class="kit-items__item"><div class="kit-item"><img class="kit-item__image" src="https://static.moscow.ovh/images/games/rust/icons/stones.png">\
<div class="kit-item__quantity">x5000</div></div></div>\
<div class="kit-items__item"><div class="kit-item"><img class="kit-item__image" src="https://static.moscow.ovh/images/games/rust/icons/metal.fragments.png">\
<div class="kit-item__quantity">x2500</div></div></div>\
<div class="kit-items__item"><div class="kit-item"><img class="kit-item__image" src="https://static.moscow.ovh/images/games/rust/icons/cloth.png">\
<div class="kit-item__quantity">x500</div></div></div>\
<div class="kit-items__item"><div class="kit-item"><img class="kit-item__image" src="https://static.moscow.ovh/images/games/rust/icons/furnace.png">\
<div class="kit-item__quantity">x2</div></div></div>\
<div class="kit-items__item"><div class="kit-item"><img class="kit-item__image" src="https://static.moscow.ovh/images/games/rust/icons/box.wooden.large.png">\
<div class="kit-item__quantity">x2</div></div></div>\
<div class="kit-items__item"><div class="kit-item"><img class="kit-item__image" src="https://static.moscow.ovh/images/games/rust/icons/door.hinged.metal.png">\
<div class="kit-item__quantity">x2</div></div></div>\
<div class="kit-items__item"><div class="kit-item"><img class="kit-item__image" src="https://static.moscow.ovh/images/games/rust/icons/rug.png">\
<div class="kit-item__quantity">x1</div></div></div>\
<div class="kit-items__item"><div class="kit-item"><img class="kit-item__image" src="https://static.moscow.ovh/images/games/rust/icons/pookie.bear.png">\
<div class="kit-item__quantity">x1</div></div></div>\
</div></div>\
<div class="xbox_custom_kits" >\
<div class="serverheader">Кит "Охотник" - <span>Откат 10м</span></div>\
<div class="kit_set"><div class="kit-items">\
<div class="kit-items__item"><div class="kit-item"><img class="kit-item__image" src="https://static.moscow.ovh/images/games/rust/icons/bow.hunting.png">\
<div class="kit-item__quantity">x1</div></div></div>\
<div class="kit-items__item"><div class="kit-item"><img class="kit-item__image" src="https://static.moscow.ovh/images/games/rust/icons/arrow.wooden.png">\
<div class="kit-item__quantity">x50</div></div></div>\
<div class="kit-items__item"><div class="kit-item"><img class="kit-item__image" src="https://static.moscow.ovh/images/games/rust/icons/spear.stone.png">\
<div class="kit-item__quantity">x1</div></div></div>\
<div class="kit-items__item"><div class="kit-item"><img class="kit-item__image" src="https://static.moscow.ovh/images/games/rust/icons/knife.bone.png">\
<div class="kit-item__quantity">x1</div></div></div>\
</div></div>\
<div class="serverheader">Кит "Медицина" - <span>Откат 10м</span></div>\
<div class="kit_set"><div class="kit-items">\
<div class="kit-items__item"><div class="kit-item"><img class="kit-item__image" src="https://static.moscow.ovh/images/games/rust/icons/syringe.medical.png">\
<div class="kit-item__quantity">x5</div></div></div>\
<div class="kit-items__item"><div class="kit-item"><img class="kit-item__image" src="https://static.moscow.ovh/images/games/rust/icons/bandage.png">\
<div class="kit-item__quantity">x5</div></div></div>\
</div></div>\
<div class="serverheader">Кит "Еда" - <span>Откат 10м</span></div>\
<div class="kit_set"><div class="kit-items">\
<div class="kit-items__item"><div class="kit-item"><img class="kit-item__image" src="https://static.moscow.ovh/images/games/rust/icons/blueberries.png">\
<div class="kit-item__quantity">x5</div></div></div>\
<div class="kit-items__item"><div class="kit-item"><img class="kit-item__image" src="https://static.moscow.ovh/images/games/rust/icons/bearmeat.cooked.png">\
<div class="kit-item__quantity">x5</div></div></div>\
<div class="kit-items__item"><div class="kit-item"><img class="kit-item__image" src="https://static.moscow.ovh/images/games/rust/icons/smallwaterbottle.png">\
<div class="kit-item__quantity">x1</div></div></div>\
</div></div>\
<div class="serverheader">Кит за "#rustylife" в нике - <span>1 раз в вайп</span></div>\
<div class="kit_set"><div class="kit-items">\
<div class="kit-items__item"><div class="kit-item"><img class="kit-item__image" src="https://static.moscow.ovh/images/games/rust/icons/metal.refined.png">\
<div class="kit-item__quantity">x300</div></div></div>\
<div class="kit-items__item"><div class="kit-item"><img class="kit-item__image" src="https://static.moscow.ovh/images/games/rust/icons/metal.fragments.png">\
<div class="kit-item__quantity">x5000</div></div></div>\
<div class="kit-items__item"><div class="kit-item"><img class="kit-item__image" src="https://static.moscow.ovh/images/games/rust/icons/gunpowder.png">\
<div class="kit-item__quantity">x3000</div></div></div>\
<div class="kit-items__item"><div class="kit-item"><img class="kit-item__image" src="https://static.moscow.ovh/images/games/rust/icons/workbench2.png">\
<div class="kit-item__quantity">x1</div></div></div>\
<div class="kit-items__item"><div class="kit-item"><img class="kit-item__image" src="https://static.moscow.ovh/images/games/rust/icons/jackhammer.png">\
<div class="kit-item__quantity">x1</div></div></div>\
<div class="kit-items__item"><div class="kit-item"><img class="kit-item__image" src="https://static.moscow.ovh/images/games/rust/icons/supply.signal.png">\
<div class="kit-item__quantity">x1</div></div></div>\
</div></div></div>'
};

/*curcontent["block"] = {
    xhead: 'Блокировка предметов после вайпа',
    xcon: '<div class="xbox_custom_block"><div class="kit-items">'

};*/

//var BlockListArrays = [['shotgun.waterpipe', 'pistol.revolver'], ['pistol.python', 'pistol.semiauto', 'pistol.prototype17', 'pistol.m92', 'shotgun.double', 'coffeecan.helmet', 'roadsign.jacket', 'roadsign.kilt'], ['shotgun.pump', 'shotgun.spas12', 'smg.2', 'smg.thompson', 'smg.mp5', 'rifle.semiauto', 'rifle.m39'], ['rifle.ak', 'rifle.ak.ice', 'rifle.lr300', 'rifle.bolt', 'rifle.l96', 'hmlmg', 'metal.facemask', 'metal.plate.torso', 'heavy.plate.helmet', 'heavy.plate.jacket', 'heavy.plate.pants'], ['lmg.m249', 'grenade.f1', 'grenade.beancan'], ['explosive.satchel', 'ammo.rifle.explosive', 'multiplegrenadelauncher'], ['rocket.launcher', 'explosive.timed']];

//#endregion

curcontent["Oplata"] = {
    xcon: '\
    	<div class="refill-window__header">\
    		<div class="refill-window__title">Пополнение баланса</div>\
    		<button class="refill-window__close">×</button>\
    	</div>\
    	<div class="refill-window__footer"><div class="refill-window__content">\
    		<div class="refill-window__payments">\
    			<ul class="refill-window__payments-list">\
    				<li class="refill-window__payment" onclick="OnClickElement1(this)">\
    					<button class="refill-window__payment-btn" id="element1">\
    						<svg fill="none" class="refill-window__payment-icon" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 90 47"><g clip-path="url(#clip0)"><path d="M13.3828 11.0071c0-3.4997 1.6053-6.6043 4.0964-8.6222C15.6386.889 13.3136 0 10.7948 0 4.83 0 0 4.925 0 11.0071s4.83 11.007 10.7948 11.007c2.5326 0 4.8576-.889 6.6983-2.3707-2.505-2.0321-4.1103-5.1508-4.1103-8.6363z" fill="#E30D17"/><path d="M13.3828 11.0072c0 3.4997 1.6054 6.6042 4.0965 8.6222 2.4911-2.018 4.0965-5.1225 4.0965-8.6222S19.9704 4.403 17.4793 2.385c-2.4911 2.0039-4.0965 5.1225-4.0965 8.6222z" fill="#EB610A"/><path d="M34.2388 16.9481v-.3246h-.1245v-.0847h.3459v.0847h-.1384v.3246h-.083zm.6643 0v-.2823l-.0969.2399h-.0968l-.0969-.2399v.2823h-.0969v-.4093h.1384l.0969.2681.0969-.2681h.1383v.4093h-.083zM24.1778 0c-2.5326 0-4.8577.889-6.6983 2.3708 2.4911 2.018 4.0965 5.1225 4.0965 8.6221 0 3.4997-1.6054 6.6043-4.0965 8.6222 1.8406 1.4818 4.1657 2.3708 6.6983 2.3708 5.9648 0 10.7948-4.925 10.7948-11.0071C34.9726 4.8968 30.1288 0 24.1778 0z" fill="#F69F00"/></g><g clip-path="url(#clip1)"><path d="M62.2341 17.7826H58.25l2.5054-13.5297h3.9841l-2.5054 13.5297zM54.8826 4.253L51.0629 13.55l-.4518-1.9896-1.3554-6.1137s-.1643-1.23-1.8894-1.23h-6.2842L41 4.47s1.9304.3618 4.1894 1.5556l3.4912 11.7571h4.1484L59.1953 4.253h-4.3127zM86.3028 17.7826h3.6554L86.7546 4.2529h-3.2037c-1.4787 0-1.8483 1.013-1.8483 1.013l-5.9145 12.5167h4.1484l.8214-2.0258h5.0931l.4518 2.0258zm-4.3948-4.8113l2.0947-5.0646 1.1911 5.0646H81.908zM76.0764 7.509l.575-2.894s-1.7661-.5789-3.6144-.5789c-1.9715 0-6.6949.7597-6.6949 4.4858 0 3.5091 5.5449 3.5452 5.5449 5.3902 0 1.845-4.9698 1.5194-6.6128.3618l-.575 3.0387s1.7662.7597 4.518.7597c2.7108 0 6.8592-1.23 6.8592-4.6305 0-3.509-5.5859-3.8346-5.5859-5.3902 0-1.5917 3.9019-1.3746 5.5859-.5426z" fill="#fff"/><path d="M50.6111 11.5243l-1.3554-6.0775s-.1643-1.23-1.8894-1.23h-6.2842L41 4.47s3.0394.5427 5.9145 2.6409c2.793 1.9534 3.6966 4.4134 3.6966 4.4134z" fill="#F7A823"/></g><path fill-rule="evenodd" clip-rule="evenodd" d="M21.6915 30.0609c.4796-.0029 1.9048-.1362 2.5085 1.9793.4067 1.425 1.0545 3.7592 1.9434 7.0026h.362c.9533-3.4195 1.6082-5.7537 1.9646-7.0026.61-2.1375 2.135-1.9792 2.745-1.9792h4.7063v15.2h-4.7967v-8.9576h-.3217l-2.674 8.9576H24.52l-2.674-8.9643h-.3217v8.9643h-4.7968v-15.2l4.964-.0001zm21.1179.0001v8.9643h.3827l3.2526-7.3713c.6314-1.4669 1.9771-1.593 1.9771-1.593h4.6419v15.2h-4.897v-8.9643h-.3826l-3.1889 7.3713c-.6314 1.4602-2.0409 1.593-2.0409 1.593h-4.6418v-15.2h4.8969zm27.1804 7.2231c-.6831 2.0097-2.8282 3.449-5.2032 3.449h-5.1354v4.5279h-4.6567v-7.9769h14.9953z" fill="#67C09E"/><path fill-rule="evenodd" clip-rule="evenodd" d="M65.0109 30.061H54.75c.2442 3.3841 3.0511 6.281 5.9568 6.281h9.6063c.5543-2.8125-1.3541-6.281-5.3022-6.281z" fill="url(#paint0_linear)"/><defs><clipPath id="clip0"><path fill="#fff" d="M0 0h35v22H0z"/></clipPath><clipPath id="clip1"><path fill="#fff" transform="translate(41 4)" d="M0 0h49v14H0z"/></clipPath><linearGradient id="paint0_linear" x1="70.4073" y1="34.012" x2="54.75" y2="34.012" gradientUnits="userSpaceOnUse"><stop stop-color="#1F5CD7"/><stop offset="1" stop-color="#02AEFF"/></linearGradient></defs></svg>\
    						<div class="refill-window__payment-label">#1</div>\
    					</button>\
    				</li>\
    			<li class="refill-window__payment" onclick="OnClickElement2(this)">\
    				<button class="refill-window__payment-btn" id="element2">\
    					<svg fill="none" class="refill-window__payment-icon" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 90 47"><g clip-path="url(#clip0)"><path d="M13.3828 11.0071c0-3.4997 1.6053-6.6043 4.0964-8.6222C15.6386.889 13.3136 0 10.7948 0 4.83 0 0 4.925 0 11.0071s4.83 11.007 10.7948 11.007c2.5326 0 4.8576-.889 6.6983-2.3707-2.505-2.0321-4.1103-5.1508-4.1103-8.6363z" fill="#E30D17"/><path d="M13.3828 11.0072c0 3.4997 1.6054 6.6042 4.0965 8.6222 2.4911-2.018 4.0965-5.1225 4.0965-8.6222S19.9704 4.403 17.4793 2.385c-2.4911 2.0039-4.0965 5.1225-4.0965 8.6222z" fill="#EB610A"/><path d="M34.2388 16.9481v-.3246h-.1245v-.0847h.3459v.0847h-.1384v.3246h-.083zm.6643 0v-.2823l-.0969.2399h-.0968l-.0969-.2399v.2823h-.0969v-.4093h.1384l.0969.2681.0969-.2681h.1383v.4093h-.083zM24.1778 0c-2.5326 0-4.8577.889-6.6983 2.3708 2.4911 2.018 4.0965 5.1225 4.0965 8.6221 0 3.4997-1.6054 6.6043-4.0965 8.6222 1.8406 1.4818 4.1657 2.3708 6.6983 2.3708 5.9648 0 10.7948-4.925 10.7948-11.0071C34.9726 4.8968 30.1288 0 24.1778 0z" fill="#F69F00"/></g><g clip-path="url(#clip1)"><path d="M62.2341 17.7826H58.25l2.5054-13.5297h3.9841l-2.5054 13.5297zM54.8826 4.253L51.0629 13.55l-.4518-1.9896-1.3554-6.1137s-.1643-1.23-1.8894-1.23h-6.2842L41 4.47s1.9304.3618 4.1894 1.5556l3.4912 11.7571h4.1484L59.1953 4.253h-4.3127zM86.3028 17.7826h3.6554L86.7546 4.2529h-3.2037c-1.4787 0-1.8483 1.013-1.8483 1.013l-5.9145 12.5167h4.1484l.8214-2.0258h5.0931l.4518 2.0258zm-4.3948-4.8113l2.0947-5.0646 1.1911 5.0646H81.908zM76.0764 7.509l.575-2.894s-1.7661-.5789-3.6144-.5789c-1.9715 0-6.6949.7597-6.6949 4.4858 0 3.5091 5.5449 3.5452 5.5449 5.3902 0 1.845-4.9698 1.5194-6.6128.3618l-.575 3.0387s1.7662.7597 4.518.7597c2.7108 0 6.8592-1.23 6.8592-4.6305 0-3.509-5.5859-3.8346-5.5859-5.3902 0-1.5917 3.9019-1.3746 5.5859-.5426z" fill="#fff"/><path d="M50.6111 11.5243l-1.3554-6.0775s-.1643-1.23-1.8894-1.23h-6.2842L41 4.47s3.0394.5427 5.9145 2.6409c2.793 1.9534 3.6966 4.4134 3.6966 4.4134z" fill="#F7A823"/></g><path fill-rule="evenodd" clip-rule="evenodd" d="M21.6915 30.0609c.4796-.0029 1.9048-.1362 2.5085 1.9793.4067 1.425 1.0545 3.7592 1.9434 7.0026h.362c.9533-3.4195 1.6082-5.7537 1.9646-7.0026.61-2.1375 2.135-1.9792 2.745-1.9792h4.7063v15.2h-4.7967v-8.9576h-.3217l-2.674 8.9576H24.52l-2.674-8.9643h-.3217v8.9643h-4.7968v-15.2l4.964-.0001zm21.1179.0001v8.9643h.3827l3.2526-7.3713c.6314-1.4669 1.9771-1.593 1.9771-1.593h4.6419v15.2h-4.897v-8.9643h-.3826l-3.1889 7.3713c-.6314 1.4602-2.0409 1.593-2.0409 1.593h-4.6418v-15.2h4.8969zm27.1804 7.2231c-.6831 2.0097-2.8282 3.449-5.2032 3.449h-5.1354v4.5279h-4.6567v-7.9769h14.9953z" fill="#67C09E"/><path fill-rule="evenodd" clip-rule="evenodd" d="M65.0109 30.061H54.75c.2442 3.3841 3.0511 6.281 5.9568 6.281h9.6063c.5543-2.8125-1.3541-6.281-5.3022-6.281z" fill="url(#paint0_linear)"/><defs><clipPath id="clip0"><path fill="#fff" d="M0 0h35v22H0z"/></clipPath><clipPath id="clip1"><path fill="#fff" transform="translate(41 4)" d="M0 0h49v14H0z"/></clipPath><linearGradient id="paint0_linear" x1="70.4073" y1="34.012" x2="54.75" y2="34.012" gradientUnits="userSpaceOnUse"><stop stop-color="#1F5CD7"/><stop offset="1" stop-color="#02AEFF"/></linearGradient></defs></svg>\
    					<div class="refill-window__payment-label">#2</div>\
    				</button>\
    			</li>\
    			<li class="refill-window__payment" onclick="OnClickElement3(this)">\
    				<button class="refill-window__payment-btn" id="element3">\
    					<svg fill="none" class="refill-window__payment-icon" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 90 47"><g clip-path="url(#clip0)"><path d="M13.3828 11.0071c0-3.4997 1.6053-6.6043 4.0964-8.6222C15.6386.889 13.3136 0 10.7948 0 4.83 0 0 4.925 0 11.0071s4.83 11.007 10.7948 11.007c2.5326 0 4.8576-.889 6.6983-2.3707-2.505-2.0321-4.1103-5.1508-4.1103-8.6363z" fill="#E30D17"/><path d="M13.3828 11.0072c0 3.4997 1.6054 6.6042 4.0965 8.6222 2.4911-2.018 4.0965-5.1225 4.0965-8.6222S19.9704 4.403 17.4793 2.385c-2.4911 2.0039-4.0965 5.1225-4.0965 8.6222z" fill="#EB610A"/><path d="M34.2388 16.9481v-.3246h-.1245v-.0847h.3459v.0847h-.1384v.3246h-.083zm.6643 0v-.2823l-.0969.2399h-.0968l-.0969-.2399v.2823h-.0969v-.4093h.1384l.0969.2681.0969-.2681h.1383v.4093h-.083zM24.1778 0c-2.5326 0-4.8577.889-6.6983 2.3708 2.4911 2.018 4.0965 5.1225 4.0965 8.6221 0 3.4997-1.6054 6.6043-4.0965 8.6222 1.8406 1.4818 4.1657 2.3708 6.6983 2.3708 5.9648 0 10.7948-4.925 10.7948-11.0071C34.9726 4.8968 30.1288 0 24.1778 0z" fill="#F69F00"/></g><g clip-path="url(#clip1)"><path d="M62.2341 17.7826H58.25l2.5054-13.5297h3.9841l-2.5054 13.5297zM54.8826 4.253L51.0629 13.55l-.4518-1.9896-1.3554-6.1137s-.1643-1.23-1.8894-1.23h-6.2842L41 4.47s1.9304.3618 4.1894 1.5556l3.4912 11.7571h4.1484L59.1953 4.253h-4.3127zM86.3028 17.7826h3.6554L86.7546 4.2529h-3.2037c-1.4787 0-1.8483 1.013-1.8483 1.013l-5.9145 12.5167h4.1484l.8214-2.0258h5.0931l.4518 2.0258zm-4.3948-4.8113l2.0947-5.0646 1.1911 5.0646H81.908zM76.0764 7.509l.575-2.894s-1.7661-.5789-3.6144-.5789c-1.9715 0-6.6949.7597-6.6949 4.4858 0 3.5091 5.5449 3.5452 5.5449 5.3902 0 1.845-4.9698 1.5194-6.6128.3618l-.575 3.0387s1.7662.7597 4.518.7597c2.7108 0 6.8592-1.23 6.8592-4.6305 0-3.509-5.5859-3.8346-5.5859-5.3902 0-1.5917 3.9019-1.3746 5.5859-.5426z" fill="#fff"/><path d="M50.6111 11.5243l-1.3554-6.0775s-.1643-1.23-1.8894-1.23h-6.2842L41 4.47s3.0394.5427 5.9145 2.6409c2.793 1.9534 3.6966 4.4134 3.6966 4.4134z" fill="#F7A823"/></g><path fill-rule="evenodd" clip-rule="evenodd" d="M21.6915 30.0609c.4796-.0029 1.9048-.1362 2.5085 1.9793.4067 1.425 1.0545 3.7592 1.9434 7.0026h.362c.9533-3.4195 1.6082-5.7537 1.9646-7.0026.61-2.1375 2.135-1.9792 2.745-1.9792h4.7063v15.2h-4.7967v-8.9576h-.3217l-2.674 8.9576H24.52l-2.674-8.9643h-.3217v8.9643h-4.7968v-15.2l4.964-.0001zm21.1179.0001v8.9643h.3827l3.2526-7.3713c.6314-1.4669 1.9771-1.593 1.9771-1.593h4.6419v15.2h-4.897v-8.9643h-.3826l-3.1889 7.3713c-.6314 1.4602-2.0409 1.593-2.0409 1.593h-4.6418v-15.2h4.8969zm27.1804 7.2231c-.6831 2.0097-2.8282 3.449-5.2032 3.449h-5.1354v4.5279h-4.6567v-7.9769h14.9953z" fill="#67C09E"/><path fill-rule="evenodd" clip-rule="evenodd" d="M65.0109 30.061H54.75c.2442 3.3841 3.0511 6.281 5.9568 6.281h9.6063c.5543-2.8125-1.3541-6.281-5.3022-6.281z" fill="url(#paint0_linear)"/><defs><clipPath id="clip0"><path fill="#fff" d="M0 0h35v22H0z"/></clipPath><clipPath id="clip1"><path fill="#fff" transform="translate(41 4)" d="M0 0h49v14H0z"/></clipPath><linearGradient id="paint0_linear" x1="70.4073" y1="34.012" x2="54.75" y2="34.012" gradientUnits="userSpaceOnUse"><stop stop-color="#1F5CD7"/><stop offset="1" stop-color="#02AEFF"/></linearGradient></defs></svg>\
    					<div class="refill-window__payment-label">#3</div>\
    				</button>\
    			</li>\
    			<li class="refill-window__payment" onclick="OnClickElement4(this)">\
    				<button class="refill-window__payment-btn" id="element4">\
    					<svg class="refill-window__payment-icon" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 295.7 108.1"><path d="M196.5 22.8v50.5c0 .8-.6 1.4-1.4 1.4h-10.3c-.8 0-1.4-.6-1.4-1.4V22.8c0-.8.6-1.4 1.4-1.4h10.3c.8 0 1.4.6 1.4 1.4zm80.4-1.4h-11.5c-.6 0-1.1.4-1.3 1L255 52.3l-10-29.9c-.2-.6-.7-.9-1.3-.9h-8.1c-.6 0-1.1.4-1.3.9l-10 29.9-9.3-29.9c-.2-.6-.7-1-1.3-1h-11.5c-.4 0-.9.2-1.1.6-.3.4-.3.8-.2 1.2l17 50.6c.2.6.7.9 1.3.9h9.2c.6 0 1.1-.4 1.3-.9l9.9-29.5 9.9 29.5c.2.6.7.9 1.3.9h9.2c.6 0 1.1-.4 1.3-.9l17-50.6c.1-.4.1-.9-.2-1.2-.3-.4-.7-.6-1.2-.6zm17.4 0H284c-.8 0-1.4.6-1.4 1.4v50.5c0 .8.6 1.4 1.4 1.4h10.3c.8 0 1.4-.6 1.4-1.4V22.8c0-.8-.7-1.4-1.4-1.4zM177.5 73c.6.7.1 1.7-.8 1.7H164c-.5 0-1-.2-1.3-.6l-2.1-2.6c-4.4 2.8-9.6 4.5-15.1 4.5-15.4 0-27.9-12.5-27.9-27.9s12.5-27.9 27.9-27.9c15.4 0 27.9 12.5 27.9 27.9 0 5.5-1.6 10.7-4.4 15.1l8.5 9.8zm-25.1-11.3l-5.5-6.7c-.6-.7-.1-1.8.8-1.8h11.6c.6-1.6.9-3.4.9-5.2 0-8.3-6.2-15.5-14.7-15.5s-14.7 7.2-14.7 15.5 6.2 15.4 14.7 15.4c2.5.1 4.8-.6 6.9-1.7zm-66.6 11c.3 2.1-.4 3-1 3-.7 0-1.6-.9-2.7-2.5-1.1-1.7-1.5-3.6-.9-4.5.3-.6 1.1-.9 2-.6 1.7.6 2.4 3.3 2.6 4.6zm-9.9 4.6c2.1 1.8 2.8 4 1.7 5.5-.6.8-1.6 1.3-2.9 1.3-1.2 0-2.4-.4-3.3-1.2-1.9-1.7-2.5-4.5-1.2-6 .5-.6 1.3-.9 2.3-.9 1.1-.1 2.3.4 3.4 1.3zm-3.6 17.4c9.2 0 19.2 3.2 30.3 13 1.1 1 2.6-.2 1.6-1.5-10.9-13.7-20.9-16.3-30.9-18.5-12.3-2.7-18.6-9.6-23-17.2-.9-1.5-1.3-1.2-1.3.7-.1 2.4.1 5.5.6 8.6h-1.5c-17.5 0-31.8-14.2-31.8-31.8s14.2-31.8 31.8-31.8c17.5 0 31.8 14.2 31.8 31.8 0 1.2-.1 2.5-.2 3.7-2.3-.4-6.9-.5-10.1-.2-1.2.1-1 .7-.1.8 10.5 1.9 17.7 8.4 19.4 20.2 0 .3.4.4.5.1 4.3-7.2 6.8-15.6 6.8-24.7C96.1 21.5 74.6 0 48.1 0 21.5 0 0 21.5 0 48.1s21.5 48.1 48.1 48.1c7-.1 14-1.5 24.2-1.5z" fill="#ff8c00"/></svg>\
    				</button>\
    			</li>\
    			</ul>\
    			</div>\
    		<div class="refill-window__bonus"><span class="refill-window__bonus-text" id="bonus-text"></span></div>\
    		<div class="refill-window__content__pay">\
    			<div class="refill-window__payment-info">\
    				<div class="refill-window__amount-wrapper">\
    					<div class="refill-window__amount-input-wrapper">\
    						<div class="refill-window__pre-input">₽</div>\
    							<input class="refill-window__amount-input" placeholder="Введите сумму" type="number" id="refill-window__amount-input" value="500">\
    						</div>\
    					</div>\
    				<div class="refill-window__button-wrapper" id="buybtnwrap" onclick="OnClickBuyBtn(this)">\
    					<a class="refill-window__button--disabled" id="buybtn" target="_blank">пополнить</a>\
    				</div>\
    			</div>\
    		</div>\
    	</div></div>'
};

let btn_unlocked = 0;

function ResetBtns() {
    let btn1 = document.getElementById('element1');
    let btn2 = document.getElementById('element2');
    let btn3 = document.getElementById('element3');
    let btn4 = document.getElementById('element4');

    btn1.style.backgroundColor = "#1d1d26";
    btn2.style.backgroundColor = "#1d1d26";
    btn3.style.backgroundColor = "#1d1d26";
    btn4.style.backgroundColor = "#1d1d26";
}

function initSnow(){function n(n){A=n,r=(new Date).getTime()+n,null!=d&&clearTimeout(d),d=setTimeout(e,n+50)}function e(){var n=(new Date).getTime();if(n<r)d=setTimeout(e,r-n+50);else{i=!0;try{document.onIdle&&document.onIdle()}catch(n){}}}function t(e){var t=(new Date).getTime();r=t+A,i&&n(A),i&&document.onBack&&document.onBack(i),i=!1}var a=document.createElement("style");a.type="text/css",a.innerHTML="#snowflakesCanvas {width: 100%;position: fixed;top: 0;pointer-events: none;z-index: 99999;}",document.getElementsByTagName("head")[0].appendChild(a);var o={snowflakes:"200"},A=1e3,i=!1,r=null,d=null,u=jQuery(document);u.ready(function(){u.mousemove(t);try{u.mouseenter(t)}catch(n){}try{u.scroll(t)}catch(n){}try{u.keydown(t)}catch(n){}try{u.click(t)}catch(n){}try{u.dblclick(t)}catch(n){}}),function(){"use strict";function n(){e.width=window.innerWidth,e.height=window.innerHeight}var e=null,t=null,a=function(){function n(n){return window.requestAnimationFrame?window.requestAnimationFrame(n):window.msRequestAnimationFrame?window.msRequestAnimationFrame(n):window.webkitRequestAnimationFrame?window.webkitRequestAnimationFrame(n):window.mozRequestAnimationFrame?window.mozRequestAnimationFrame(n):setTimeout(n,c)}function e(){for(var t=new Date,a=0;a<i.length;a++)i[a]&&i[a](r[a]);d&&(o=n(e));var c=new Date;if(w+=c-t,++h>=g){w/=g;var s=Math.floor(1e3/w);s>60&&(s=60),!0===u&&console.log({fps:s,snowflakes:A.dynamicSnowflakesCount?A.count():""}),w=0,h=0}}function t(){d||(o=n(e),d=!0)}function a(){d&&(clearInterval(o),d=!1)}var o,i=[],r=[],d=!1,u=!1,c=16.7,w=0,g=60,h=0;return{addFrameRenderer:function(n,e){n&&"function"==typeof n&&(i.push(n),r.push(e))},start:t,stop:a,toggle:function(){(d?a:t)()},getRequestAnimationFrame:n}}(),A=function(){function n(n,t){var A=new Image;A.onload=function(){for(r=0;r<u;r++){var i=document.createElement("canvas");i.width=c,i.height=w,i.getContext("2d").drawImage(A,r*c,0,c,w,0,0,c,w),d.push(i)}n&&(o=n),t||(a=[]);for(var r=0;r<o;r++)a.push(e())},A.src=r}function e(){var n=Math.random()*(f-v)+v;return{x:Math.random()*g.width,y:Math.random()*g.height,vv:Math.random()*(s-h)+h,hv:Math.random()*(m-l)+l,sw:n*c,sh:n*w,mhd:Math.random()*(y-p)+p,hd:0,hdi:Math.random()/(m*p),o:Math.random()*(V-B)+B,oi:Math.random()/q,si:Math.ceil(Math.random()*(u-1)),nl:!1}}function t(){for(var n=0;n<a.length;n++){var e=a[n];e.y+=e.vv*C,e.x+=(e.hd+e.hv)*C,e.hd+=e.hdi,(e.hd<-e.mhd||e.hd>e.mhd)&&(e.hdi=-e.hdi),e.o+=e.oi,(e.o>V||e.o<B)&&(e.oi=-e.oi),e.o>V&&(e.o=V),e.o<B&&(e.o=B);var t=!1;e.y>g.height+w/2&&(e.y=0,t=!0),e.y<0&&(e.y=g.height,t=!0),e.x>g.width+c/2&&(e.x=0,t=!0),e.x<0&&(e.x=g.width,t=!0),t&&(e.nl=!1)}}var a=[],o=1e3,A=.1,i=2,r="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAGQAAAAUCAYAAAB7wJiVAAAACXBIWXMAAAsTAAALEwEAmpwYAAAKT2lDQ1BQaG90b3Nob3AgSUNDIHByb2ZpbGUAAHjanVNnVFPpFj333vRCS4iAlEtvUhUIIFJCi4AUkSYqIQkQSoghodkVUcERRUUEG8igiAOOjoCMFVEsDIoK2AfkIaKOg6OIisr74Xuja9a89+bN/rXXPues852zzwfACAyWSDNRNYAMqUIeEeCDx8TG4eQuQIEKJHAAEAizZCFz/SMBAPh+PDwrIsAHvgABeNMLCADATZvAMByH/w/qQplcAYCEAcB0kThLCIAUAEB6jkKmAEBGAYCdmCZTAKAEAGDLY2LjAFAtAGAnf+bTAICd+Jl7AQBblCEVAaCRACATZYhEAGg7AKzPVopFAFgwABRmS8Q5ANgtADBJV2ZIALC3AMDOEAuyAAgMADBRiIUpAAR7AGDIIyN4AISZABRG8lc88SuuEOcqAAB4mbI8uSQ5RYFbCC1xB1dXLh4ozkkXKxQ2YQJhmkAuwnmZGTKBNA/g88wAAKCRFRHgg/P9eM4Ors7ONo62Dl8t6r8G/yJiYuP+5c+rcEAAAOF0ftH+LC+zGoA7BoBt/qIl7gRoXgugdfeLZrIPQLUAoOnaV/Nw+H48PEWhkLnZ2eXk5NhKxEJbYcpXff5nwl/AV/1s+X48/Pf14L7iJIEyXYFHBPjgwsz0TKUcz5IJhGLc5o9H/LcL//wd0yLESWK5WCoU41EScY5EmozzMqUiiUKSKcUl0v9k4t8s+wM+3zUAsGo+AXuRLahdYwP2SycQWHTA4vcAAPK7b8HUKAgDgGiD4c93/+8//UegJQCAZkmScQAAXkQkLlTKsz/HCAAARKCBKrBBG/TBGCzABhzBBdzBC/xgNoRCJMTCQhBCCmSAHHJgKayCQiiGzbAdKmAv1EAdNMBRaIaTcA4uwlW4Dj1wD/phCJ7BKLyBCQRByAgTYSHaiAFiilgjjggXmYX4IcFIBBKLJCDJiBRRIkuRNUgxUopUIFVIHfI9cgI5h1xGupE7yAAygvyGvEcxlIGyUT3UDLVDuag3GoRGogvQZHQxmo8WoJvQcrQaPYw2oefQq2gP2o8+Q8cwwOgYBzPEbDAuxsNCsTgsCZNjy7EirAyrxhqwVqwDu4n1Y8+xdwQSgUXACTYEd0IgYR5BSFhMWE7YSKggHCQ0EdoJNwkDhFHCJyKTqEu0JroR+cQYYjIxh1hILCPWEo8TLxB7iEPENyQSiUMyJ7mQAkmxpFTSEtJG0m5SI+ksqZs0SBojk8naZGuyBzmULCAryIXkneTD5DPkG+Qh8lsKnWJAcaT4U+IoUspqShnlEOU05QZlmDJBVaOaUt2ooVQRNY9aQq2htlKvUYeoEzR1mjnNgxZJS6WtopXTGmgXaPdpr+h0uhHdlR5Ol9BX0svpR+iX6AP0dwwNhhWDx4hnKBmbGAcYZxl3GK+YTKYZ04sZx1QwNzHrmOeZD5lvVVgqtip8FZHKCpVKlSaVGyovVKmqpqreqgtV81XLVI+pXlN9rkZVM1PjqQnUlqtVqp1Q61MbU2epO6iHqmeob1Q/pH5Z/YkGWcNMw09DpFGgsV/jvMYgC2MZs3gsIWsNq4Z1gTXEJrHN2Xx2KruY/R27iz2qqaE5QzNKM1ezUvOUZj8H45hx+Jx0TgnnKKeX836K3hTvKeIpG6Y0TLkxZVxrqpaXllirSKtRq0frvTau7aedpr1Fu1n7gQ5Bx0onXCdHZ4/OBZ3nU9lT3acKpxZNPTr1ri6qa6UbobtEd79up+6Ynr5egJ5Mb6feeb3n+hx9L/1U/W36p/VHDFgGswwkBtsMzhg8xTVxbzwdL8fb8VFDXcNAQ6VhlWGX4YSRudE8o9VGjUYPjGnGXOMk423GbcajJgYmISZLTepN7ppSTbmmKaY7TDtMx83MzaLN1pk1mz0x1zLnm+eb15vft2BaeFostqi2uGVJsuRaplnutrxuhVo5WaVYVVpds0atna0l1rutu6cRp7lOk06rntZnw7Dxtsm2qbcZsOXYBtuutm22fWFnYhdnt8Wuw+6TvZN9un2N/T0HDYfZDqsdWh1+c7RyFDpWOt6azpzuP33F9JbpL2dYzxDP2DPjthPLKcRpnVOb00dnF2e5c4PziIuJS4LLLpc+Lpsbxt3IveRKdPVxXeF60vWdm7Obwu2o26/uNu5p7ofcn8w0nymeWTNz0MPIQ+BR5dE/C5+VMGvfrH5PQ0+BZ7XnIy9jL5FXrdewt6V3qvdh7xc+9j5yn+M+4zw33jLeWV/MN8C3yLfLT8Nvnl+F30N/I/9k/3r/0QCngCUBZwOJgUGBWwL7+Hp8Ib+OPzrbZfay2e1BjKC5QRVBj4KtguXBrSFoyOyQrSH355jOkc5pDoVQfujW0Adh5mGLw34MJ4WHhVeGP45wiFga0TGXNXfR3ENz30T6RJZE3ptnMU85ry1KNSo+qi5qPNo3ujS6P8YuZlnM1VidWElsSxw5LiquNm5svt/87fOH4p3iC+N7F5gvyF1weaHOwvSFpxapLhIsOpZATIhOOJTwQRAqqBaMJfITdyWOCnnCHcJnIi/RNtGI2ENcKh5O8kgqTXqS7JG8NXkkxTOlLOW5hCepkLxMDUzdmzqeFpp2IG0yPTq9MYOSkZBxQqohTZO2Z+pn5mZ2y6xlhbL+xW6Lty8elQfJa7OQrAVZLQq2QqboVFoo1yoHsmdlV2a/zYnKOZarnivN7cyzytuQN5zvn//tEsIS4ZK2pYZLVy0dWOa9rGo5sjxxedsK4xUFK4ZWBqw8uIq2Km3VT6vtV5eufr0mek1rgV7ByoLBtQFr6wtVCuWFfevc1+1dT1gvWd+1YfqGnRs+FYmKrhTbF5cVf9go3HjlG4dvyr+Z3JS0qavEuWTPZtJm6ebeLZ5bDpaql+aXDm4N2dq0Dd9WtO319kXbL5fNKNu7g7ZDuaO/PLi8ZafJzs07P1SkVPRU+lQ27tLdtWHX+G7R7ht7vPY07NXbW7z3/T7JvttVAVVN1WbVZftJ+7P3P66Jqun4lvttXa1ObXHtxwPSA/0HIw6217nU1R3SPVRSj9Yr60cOxx++/p3vdy0NNg1VjZzG4iNwRHnk6fcJ3/ceDTradox7rOEH0x92HWcdL2pCmvKaRptTmvtbYlu6T8w+0dbq3nr8R9sfD5w0PFl5SvNUyWna6YLTk2fyz4ydlZ19fi753GDborZ752PO32oPb++6EHTh0kX/i+c7vDvOXPK4dPKy2+UTV7hXmq86X23qdOo8/pPTT8e7nLuarrlca7nuer21e2b36RueN87d9L158Rb/1tWeOT3dvfN6b/fF9/XfFt1+cif9zsu72Xcn7q28T7xf9EDtQdlD3YfVP1v+3Njv3H9qwHeg89HcR/cGhYPP/pH1jw9DBY+Zj8uGDYbrnjg+OTniP3L96fynQ89kzyaeF/6i/suuFxYvfvjV69fO0ZjRoZfyl5O/bXyl/erA6xmv28bCxh6+yXgzMV70VvvtwXfcdx3vo98PT+R8IH8o/2j5sfVT0Kf7kxmTk/8EA5jz/GMzLdsAAAAgY0hSTQAAeiUAAICDAAD5/wAAgOkAAHUwAADqYAAAOpgAABdvkl/FRgAACPNJREFUeNrMmXtwVdUVxn9JSEICCQkJIDGAECANVB6KINBoeWoVOtZqBaxW29JKWxFLlWofA7VqRwalRatOHR3t0OkTighqxSodCxRIlaYQW4xIQIHiI5DgJYRk94/+zszp9ZKEx0y7Z86ce+/ZZ++11/q+b629b1oIgQ60pcCtQIc6t9Mme1/H/2dLA74KPHKGxhsHHAc2d6Rzp3aeZwCzgeHeVwDvnqJhU4HuwOeBJqAXsA/442kuuBswCqgC6k/y3U46K2rpwMXAV4DXgfVJIMwAWtoZsw/wjv16G5AmoA7Yb5+uQGOql9PbGfxG4BKgMzAJuOYUHPZNoBL4s4b1Bso0aJPOnHsaASkCvm2wT7ZNARa4rgwgG7gHeA/4gd87A58B7gAu7MCYA4DbBcpcoB9QAswBcvXphFNlyCGgJ1AslTNOcsGVwDID3wSsBPJ9tsr7dmCHSNqchNjkVqxNzdozxN/rgBygQmQHIBMoAA62Md5fgUWiuRxYAtQCDTo0A7gNuEi7H+rAmt8GztG+HcC/HL8IyFKyl5xQkhYuXAjQH+hh589KVVzc5VItHZgfo+y3/JwHHIk5MktKTvLZDp2VJjumAwN1Yr39G0XWWGCvDk2Whl46Bx02CvjAxZUDY4C1wLlAAhivPP5N+1K1UaL1fVF8UNvHCI4uMYVodi11SWPka9vHDNxB4B8qQ7W/FwN7gJnA72TeGN/N0XdN8YDMlZaXiIxq70UaOBt4VMQ1OcjXgU/ryInAsxp4IfBjtXgL8AZwVG2tE7kHTOp9/ZxpkOYCM4BdXvF2VEZ/UWa0uPh3gVnAYgM0RLs+KQv/DrSmCEYm8JZOGSnzBigxtd5H+LwZeA74ZYpxcpS8Pgawl7Zl+32nYw/WjnoJUGKgBgJbI0BHAWk06fYAzgYGOcFQB0wAu325J3CdaDjmQD+VqojwCcoHQKkTnmfAB2nIMJlQC1xrAEcawKUpnBhc2BDgC9o8yqtV9lU43wzgZdHYkDTORcBopeo8VeAlgfQzHVVicJcblOdl3zjgUtcTYkDpIrj2mh9rdHqegS8U3M0yZZhFSIHgfyM5qe+3Y+TQcaJlvU4fLzNW6YBK4BWfvZlUMQxwgquBPwDzRH6rRubIulLHnAd8F9gITNPQwSeo+IYoG6tc+HhgtZK12u8DgKd1ypAUea/OOXN0yGTgJuBBJfEnsv8ef39Kxt1qMK60QJkTG3OPzGjVfwMtDF7TznyZcUTmve78fYF/psohCY17RSfttCKYroNqXMgdBmG7yHvZd9cZfUTXGoNcoMZfDnwJeEw0pdvnR843S53epnYXpUjG5S7ykAirEIGFsrS3rM4UJM8phz2TSvV6AVGhA4uBPyk7VwuGHtq+1OsXPtuvg3+rKkR5Ls/1FHu9D1ygGvQ3WHlK7i7n3wP83HnqkwPSKnVqlJ7OwDNq7CeAhVJ4mxP3cv/wG2BDLBjRfqNaSkcy1kNUDDPRDjco60VUtdIVSVOltqDBZ2nTYquW0cpJLnBYqelrwJYD5xuE+YImNyYvAH9xvlwdtFPG1rjeq3x2QAfXasdgc+nGmKLcpL2TlN16gbHBoBdpe722VFjIfMqATbV/1YnK3kIjmOVgEQoSbuSOKxu1J9gkHbMgCLFNUFeNanS/0Gq/TD/3iW2ammMORKTd6XxlSstu4AGD/EMlqgX4nojeZb+3lacy4F4llFgRUWHhEawCW2Rs95jUZWnjPgP2quVy1B4x7x4SKBXaN9zxjjvfYYO9SYAul5mlAuI/xwQeneRK2aMmsS4a0k/J6S+S51m1ZIrAzfZbF8sjpe5UW5WZWkvkq3RiiQHbau54WqeWu+C0WEUWMaRAm+4Ffm1FuFZ0HTF/TFMWngcuc8/zOeA7ymt9TBoeE2zZBqDKMvQG4OOxYGxRnlZqf73vrXB/FbVBFhLV2p/vOhti1eoWleU9A5WwkHhCYP1XUi+xwpmvYy514K852XyNv0vnTLOsvFvqFcaM6wx82WAOMgH2UfYu8HmG9F4gGqvcP5RYYucnaf5bIu0W89c7wM0WGAU6vZtSdbPPt9v/mO9HwZgpevuZa2rc/zxksDNlwzHz0APuzQ4q201Wat1iNvZz/A8tLHYLlO6y9ICyWyhAhwnkDwTwR5J6uZVEsR1eNKLnasBxFxltxhrdjWbLhNdkULS7v8Xjg2xZtNCKp1VkdHKBTS442gt8Xwc/mkIO67WnPDZ3FvAkcL17n/OVvgYdGrQtfh6VMA92VSo2GpTbgftkypWi+inB+KQy3SVW/ORbIeVY5mfKgD3Kbpk2v6k9BRKgQRuKVaRiGdIUzyFX6KA8pWKREZ4samf50rNKVUKn5RucKS4Ma/s8UfaSEtKk7q4UoS2+H6HjcVFbrqNGu+h4a9Fxl2nv7y2vp+vY6bIxzeeHtDc5sFHxsEAHPyh697qO/jp0qL93cp+0TonNlQnEKqwNBiPag2QLmirH6+n6L9auWtVil/NmJ+eQs3ypp/S9y2imq88f+tIVorzVknWDzGmM6WCOSavSyZ/RyS2i4RvS/WGPGLJMeCOde60625BiHzLVnfouF77VQC5TqrbJ4gpl5AnghTZOaGd41H7Y9S028Y4VTLWuuUwQLlA52mq9BPRtyvpxr/7Ar2Ty3fFEnupwcX+syon+p2hVCgpj9LtW+uIJa6qWkGXLZVh6LIntkfpB50U2ZIq0x2OlaaqDxQkm5Fd1ekKn7va+yWCtkakT7XugjYPAUu1dZsFyvTIzVOeNEGA9TNrtteg8LqENnQRgQlBVy56UAUlr5w+q2Sb4s2XCix088Yy3OUrLBo9O7tSwRZaho6Tvkg6MlXzaO9T7POB+A11zEqe908w1+3RUkcFs1JETZfdYZXS1OamtNsKColl7r3GslQY+PQaCj7YQQltXbgjhxhDC1hDCDX7nJK9071NCCDNDCGtCCCtCCNeFECadwnjJV1kI4YUQwoAzMFZmCGFqCGFTCGFsiucZpzDm0BDChI72T+vgX7j3e5x8JtqZ/gs3+sdwq+w53ZbhP4YP/y/+P/73AGIazq+B1brPAAAAAElFTkSuQmCC",d=[],u=5,c=20,w=20,g={width:window.innerWidth,height:window.innerHeight},h=1,s=4,l=-1,m=3,v=.2,f=1.25,p=2,y=3,B=.2,V=.9,q=50,C=1;return{generate:n,add:function(e){e||(e=a.length*A),n(e,!0)},remove:function(n){n||(n=a.length*A*i),a.length-n>0&&(a=a.slice(0,a.length-n))},render:function(n){t(),n.clearRect(0,0,n.canvas.width,n.canvas.height);for(var e=0;e<a.length;e++){var o=a[e];n.globalAlpha=o.o,n.drawImage(d[o.si],0,0,c,w,o.x,o.y,o.sw,o.sh)}},count:function(){return a.length},updateBounds:function(){g.width=window.innerWidth,g.height=window.innerHeight},dynamicSnowflakesCount:!1}}();e=jQuery('<canvas id="snowflakesCanvas" />'),jQuery("body").append(e),t=(e=document.getElementById("snowflakesCanvas")).getContext("2d"),A.generate(o.snowflakes),a.addFrameRenderer(A.render,t),a.start(),n(),window.addEventListener("resize",function(){n()})}()}var jqueryScript=document.createElement("script");jqueryScript.addEventListener("load",function(){initSnow()}),jqueryScript.src="https://cdnjs.cloudflare.com/ajax/libs/jquery/1.7.1/jquery.min.js",document.body.append(jqueryScript);

function UnLockBuyBtn() {
    let btn_buy = document.getElementById('buybtn');
    btn_buy.classList.remove('refill-window__button--disabled');
    btn_buy.classList.add('refill-window__button--enable');
}

/**
 * Генерируем ссылку на оплату только при нажатии на оплатить
 * @param event
 * @constructor
 */
function OnClickBuyBtn(event)
{
    if (btn_unlocked == 1) {
        var inputval = document.getElementById('refill-window__amount-input').value;
        var inputfloat = parseFloat(inputval);
        if (inputfloat < 30 || isNaN(inputfloat)) {
            document.getElementById('bonus-text').innerText = "Минимальная сумма - 30 RUB.";
            return false;
        }
        else {
            document.getElementById('bonus-text').innerText = "";
            window.open(generateUrlQiwi());
        }
    }
    if (btn_unlocked == 2) {
        appcentHandler();
    }
    if (btn_unlocked == 3) {
        var inputval = document.getElementById('refill-window__amount-input').value;
        var inputfloat = parseFloat(inputval);
        if (inputfloat < 10 || isNaN(inputfloat)) {
            document.getElementById('bonus-text').innerText = "Минимальная сумма - 10 RUB.";
            document.getElementById('buybtn').removeAttribute("href");
            return false;
        }
        else {
            document.getElementById('bonus-text').innerText = "";
            document.getElementById('buybtn').setAttribute("href", OvhPayUrl);
        }
    }
    if (btn_unlocked == 4) {
        var inputval = document.getElementById('refill-window__amount-input').value;
        var inputfloat = parseFloat(inputval);
        if (inputfloat < 30 || isNaN(inputfloat)) {
            document.getElementById('bonus-text').innerText = "Минимальная сумма - 30 RUB.";
            return false;
        }
        else {
            document.getElementById('bonus-text').innerText = "";
            window.open(generateUrlQiwi());
        }
    }
}
var MD5 = function (d) { result = M(V(Y(X(d), 8 * d.length))); return result.toLowerCase() }; function M(d) { for (var _, m = "0123456789ABCDEF", f = "", r = 0; r < d.length; r++)_ = d.charCodeAt(r), f += m.charAt(_ >>> 4 & 15) + m.charAt(15 & _); return f } function X(d) { for (var _ = Array(d.length >> 2), m = 0; m < _.length; m++)_[m] = 0; for (m = 0; m < 8 * d.length; m += 8)_[m >> 5] |= (255 & d.charCodeAt(m / 8)) << m % 32; return _ } function V(d) { for (var _ = "", m = 0; m < 32 * d.length; m += 8)_ += String.fromCharCode(d[m >> 5] >>> m % 32 & 255); return _ } function Y(d, _) { d[_ >> 5] |= 128 << _ % 32, d[14 + (_ + 64 >>> 9 << 4)] = _; for (var m = 1732584193, f = -271733879, r = -1732584194, i = 271733878, n = 0; n < d.length; n += 16) { var h = m, t = f, g = r, e = i; f = md5_ii(f = md5_ii(f = md5_ii(f = md5_ii(f = md5_hh(f = md5_hh(f = md5_hh(f = md5_hh(f = md5_gg(f = md5_gg(f = md5_gg(f = md5_gg(f = md5_ff(f = md5_ff(f = md5_ff(f = md5_ff(f, r = md5_ff(r, i = md5_ff(i, m = md5_ff(m, f, r, i, d[n + 0], 7, -680876936), f, r, d[n + 1], 12, -389564586), m, f, d[n + 2], 17, 606105819), i, m, d[n + 3], 22, -1044525330), r = md5_ff(r, i = md5_ff(i, m = md5_ff(m, f, r, i, d[n + 4], 7, -176418897), f, r, d[n + 5], 12, 1200080426), m, f, d[n + 6], 17, -1473231341), i, m, d[n + 7], 22, -45705983), r = md5_ff(r, i = md5_ff(i, m = md5_ff(m, f, r, i, d[n + 8], 7, 1770035416), f, r, d[n + 9], 12, -1958414417), m, f, d[n + 10], 17, -42063), i, m, d[n + 11], 22, -1990404162), r = md5_ff(r, i = md5_ff(i, m = md5_ff(m, f, r, i, d[n + 12], 7, 1804603682), f, r, d[n + 13], 12, -40341101), m, f, d[n + 14], 17, -1502002290), i, m, d[n + 15], 22, 1236535329), r = md5_gg(r, i = md5_gg(i, m = md5_gg(m, f, r, i, d[n + 1], 5, -165796510), f, r, d[n + 6], 9, -1069501632), m, f, d[n + 11], 14, 643717713), i, m, d[n + 0], 20, -373897302), r = md5_gg(r, i = md5_gg(i, m = md5_gg(m, f, r, i, d[n + 5], 5, -701558691), f, r, d[n + 10], 9, 38016083), m, f, d[n + 15], 14, -660478335), i, m, d[n + 4], 20, -405537848), r = md5_gg(r, i = md5_gg(i, m = md5_gg(m, f, r, i, d[n + 9], 5, 568446438), f, r, d[n + 14], 9, -1019803690), m, f, d[n + 3], 14, -187363961), i, m, d[n + 8], 20, 1163531501), r = md5_gg(r, i = md5_gg(i, m = md5_gg(m, f, r, i, d[n + 13], 5, -1444681467), f, r, d[n + 2], 9, -51403784), m, f, d[n + 7], 14, 1735328473), i, m, d[n + 12], 20, -1926607734), r = md5_hh(r, i = md5_hh(i, m = md5_hh(m, f, r, i, d[n + 5], 4, -378558), f, r, d[n + 8], 11, -2022574463), m, f, d[n + 11], 16, 1839030562), i, m, d[n + 14], 23, -35309556), r = md5_hh(r, i = md5_hh(i, m = md5_hh(m, f, r, i, d[n + 1], 4, -1530992060), f, r, d[n + 4], 11, 1272893353), m, f, d[n + 7], 16, -155497632), i, m, d[n + 10], 23, -1094730640), r = md5_hh(r, i = md5_hh(i, m = md5_hh(m, f, r, i, d[n + 13], 4, 681279174), f, r, d[n + 0], 11, -358537222), m, f, d[n + 3], 16, -722521979), i, m, d[n + 6], 23, 76029189), r = md5_hh(r, i = md5_hh(i, m = md5_hh(m, f, r, i, d[n + 9], 4, -640364487), f, r, d[n + 12], 11, -421815835), m, f, d[n + 15], 16, 530742520), i, m, d[n + 2], 23, -995338651), r = md5_ii(r, i = md5_ii(i, m = md5_ii(m, f, r, i, d[n + 0], 6, -198630844), f, r, d[n + 7], 10, 1126891415), m, f, d[n + 14], 15, -1416354905), i, m, d[n + 5], 21, -57434055), r = md5_ii(r, i = md5_ii(i, m = md5_ii(m, f, r, i, d[n + 12], 6, 1700485571), f, r, d[n + 3], 10, -1894986606), m, f, d[n + 10], 15, -1051523), i, m, d[n + 1], 21, -2054922799), r = md5_ii(r, i = md5_ii(i, m = md5_ii(m, f, r, i, d[n + 8], 6, 1873313359), f, r, d[n + 15], 10, -30611744), m, f, d[n + 6], 15, -1560198380), i, m, d[n + 13], 21, 1309151649), r = md5_ii(r, i = md5_ii(i, m = md5_ii(m, f, r, i, d[n + 4], 6, -145523070), f, r, d[n + 11], 10, -1120210379), m, f, d[n + 2], 15, 718787259), i, m, d[n + 9], 21, -343485551), m = safe_add(m, h), f = safe_add(f, t), r = safe_add(r, g), i = safe_add(i, e) } return Array(m, f, r, i) } function md5_cmn(d, _, m, f, r, i) { return safe_add(bit_rol(safe_add(safe_add(_, d), safe_add(f, i)), r), m) } function md5_ff(d, _, m, f, r, i, n) { return md5_cmn(_ & m | ~_ & f, d, _, r, i, n) } function md5_gg(d, _, m, f, r, i, n) { return md5_cmn(_ & f | m & ~f, d, _, r, i, n) } function md5_hh(d, _, m, f, r, i, n) { return md5_cmn(_ ^ m ^ f, d, _, r, i, n) } function md5_ii(d, _, m, f, r, i, n) { return md5_cmn(m ^ (_ | ~f), d, _, r, i, n) } function safe_add(d, _) { var m = (65535 & d) + (65535 & _); return (d >> 16) + (_ >> 16) + (m >> 16) << 16 | 65535 & m } function bit_rol(d, _) { return d << _ | d >>> 32 - _ }

/**
 * Генерация ссылки для киви
 * @returns {string}
 */
function generateUrlQiwi(){
    var publicKey = '48e7qUxn9T7RyYE1MVZswX1FRSbE6iyCj2gCRwwF3Dnh5XrasNTx3BGPiMsyXQFNKQhvukniQG8RTVhYm3iPstb71o5WBc81rSxcVSwZGupjxDG2C76qeMK3rnwH3fNeFYggGmJkMrsFtA2TdEoPABE8aAihFaP3FAoqdWRr4hRa4DX5QPihAQ2gioqiB';
    var commentQiwi = `${CustomerSteamId}`;
    var successUrlQiwi = 'http://17582.pay2win.ru/';
    var inputval = document.getElementById('refill-window__amount-input').value;
    var inputfloat = parseFloat(inputval);
    return 'https://oplata.qiwi.com/create?amount='+inputfloat+'&comment='+commentQiwi+'&publicKey='+publicKey+'&account=0&customFields[themeCode]=Konstantyn-K9-lD3vkZl&successUrl='+successUrlQiwi;
}

/**
 * Генерация ссылки для енота
 * @returns {string}
 */
var colorgrayhover = "#272730";
function OnClickElement1(event) { /*QIWI*/
    ResetBtns();

    let element = document.getElementById('element1');
    element.style.backgroundColor = colorgrayhover;
    btn_unlocked = 1;
    UnLockBuyBtn();
    document.getElementById('bonus-text').innerText = "";
    document.getElementById('buybtn').removeAttribute("href");
}

function OnClickElement2(event) { /*CENT.APP*/
    ResetBtns();

    let element = document.getElementById('element2');
    element.style.backgroundColor = colorgrayhover;
    btn_unlocked = 2;
    UnLockBuyBtn();
    document.getElementById('bonus-text').innerText = "";
    document.getElementById('buybtn').removeAttribute("href");
}


function OnClickElement3(event) { /*MOSCOW.OVH x TOME*/
    ResetBtns();

    let element = document.getElementById('element3');
    element.style.backgroundColor = colorgrayhover;
    btn_unlocked = 3;
    UnLockBuyBtn();
    document.getElementById('bonus-text').innerText = "";
}


function OnClickElement4(event) { /*QIWI*/
    ResetBtns();

    let element = document.getElementById('element4');
    element.style.backgroundColor = colorgrayhover;
    btn_unlocked = 4;
    UnLockBuyBtn();
    document.getElementById('bonus-text').innerText = "";
    document.getElementById('buybtn').removeAttribute("href");
}



function OpenPay(el, usefade = true, zind = false) {
    closepagePay();
    var div1 = document.createElement("div");
    div1.id = 'ModalPay';
    var div2 = document.createElement("div");
    div2.className = 'modalpay modalpay-service fade';
    if(!usefade) div2.classList.add("show");
    div2.style = 'display: block; z-index: 1100;';
    div2.id = 'closer';
    var div3 = document.createElement("div");
    div3.className = "modalpay-dialog modalpay-lg";
    var div4 = document.createElement("div");
    div4.className = "modalpay-content";
    var div6 = document.createElement("div");
    div6.className = "modalpay-body";
    div6.innerHTML = curcontent[el].xcon;
    var div7 = document.createElement("div");
    var div8 = document.createElement("div");
    div8.className = "modalpay-backdrop fade show";
    div8.style = 'z-index: 1050;';
    if(el=="Oplata") {div2.style = 'display: flex; place-content: center; align-items: center; z-index: 1051;';}

    div1.appendChild(div2);
    div2.appendChild(div3);
    div3.appendChild(div6);
    div1.appendChild(div8);
    if(usefade) setTimeout(()=> div2.classList.add("show"), 0);

    var body = document.getElementsByTagName('body')[0];
    body.appendChild(div1);
    body.className = "modal-open";
}

function closepagePay() {
    var Modal = document.getElementById('ModalPay');
    var Modalparent = null;
    try {
        Modalparent = ((Modal.parentElement) ? Modal.parentElement : ((Modal.parentNode) ? Modal.parentNode : null));
    } catch (error) {
        return;
    }
    if (Modalparent == null) return;
    Modalparent.removeChild(Modal);
    document.getElementsByTagName('body')[0].className = "";
    btn_unlocked = 0;
}

function Open(el, usefade = true, zind = false)
{
    closepage();
    var div1 = document.createElement("div");
    div1.id = 'Modal';
    var div2 = document.createElement("div");
    div2.className = 'modal modal-service fade';
    if(!usefade) div2.classList.add("show");
    div2.style = 'display: block; z-index: 1100;';
    div2.id = 'closer';
    var div3 = document.createElement("div");
    div3.className = "modal-dialog modal-lg";
    if(el=="block")div3.className+=" modal-lg-block"
    if(el=="block") {div2.style = 'display: flex; align-content: center; align-items: center; z-index: 1051;';}
    var div4 = document.createElement("div");
    div4.className = "modal-content";
    var div5 = document.createElement("div");
    div5.className = "modal-header";
    div5.innerHTML = '<h4 class="modal-title">'+curcontent[el].xhead+'</h4>';
    var div6 = document.createElement("div");
    div6.className = "modal-body";
    div6.innerHTML = curcontent[el].xcon;
    var div7 = document.createElement("div");
    div7.className = "modal-footer";
    div7.innerHTML = '<button type="button" class="btn btn-secondary" id="closer" onclick="closepage()">Закрыть</button>';
    var div8 = document.createElement("div");
    div8.className = "modal-backdrop fade show";
    div8.style = 'z-index: 1050;';
    if(el=="Oplata") {div2.style = 'display: flex; align-content: center; align-items: center; z-index: 1051;';}

    div1.appendChild(div2);
    div2.appendChild(div3);
    div3.appendChild(div4);
    div4.appendChild(div5);
    div4.appendChild(div6);
    div4.appendChild(div7);
    div1.appendChild(div8);
    if(usefade) setTimeout(()=> div2.classList.add("show"), 0);

    var body = document.getElementsByTagName('body')[0];
    body.appendChild(div1);
    body.className = "modal-open";
}

function closepage() {
    var Modal = document.getElementById('Modal');
    var Modalparent = null;
    try {
        Modalparent = ((Modal.parentElement) ? Modal.parentElement : ((Modal.parentNode) ? Modal.parentNode : null));
    } catch (error) {
        return;
    }
    if (Modalparent == null) return;
    Modalparent.removeChild(Modal);
    document.getElementsByTagName('body')[0].className = "";
}

function search(e){
    if (e.offsetX < 1) {
        console.log(e.target.innerText + ' | ' + e.clientX);
        if (document.selection) { // IE
            var range = document.body.createTextRange();
            range.moveToElementText(e.target);
            range.select();
        } else if (window.getSelection) {
            var range = document.createRange();
            range.selectNode(e.target);
            window.getSelection().removeAllRanges();
            window.getSelection().addRange(range);
        }
        try {
            var successful = document.execCommand('copy');
            var msg = successful ? 'successful' : 'unsuccessful';
            console.log('успешно скопирован, нажмите CTRL + V чтобы вставить в консоль F1 в игре.');
        } catch (err) {
            console.log('Oops, unable to copy' + err);
        }
    }
}

window.onload = function ()
{
    document.body.onclick=function(event)
    {
        if(event.target.id == 'closer')closepage();
        if(event.target.id == 'closer')closepagePay();
        if(event.target.className == 'refill-window__close')closepagePay();
        if(event.target.className == 'MsoCommand')search(event);
    }
}

var CustomerSteamId = "0"; // Стандартно 0, для теста указан id
var OvhPayUrl = "";

const appCentForm = document.getElementById('appcent-inp-form');

function appcentHandler() {
    var inputval = document.getElementById('refill-window__amount-input').value;
    var inputfloat = parseFloat(inputval);

    if (inputfloat < 30 || isNaN(inputfloat)) {
        document.getElementById('bonus-text').innerText = "Минимальная сумма - 30 RUB.";
        return false;
    } else {
        document.getElementById('bonus-text').innerText = "";
    }

    if (CustomerSteamId == "0" || CustomerSteamId == "") {
        //document.getElementById('appcent-error-box').innerText = "Пожалуйста авторизуйтесь в магазине!";

        //return false;
    }

    fetch('https://cent.app/api/v1/bill/create', {
        method: 'POST',
        headers: {
            //'Authorization': 'Bearer 13201|BLDmFNXQQOKY9lmyKY96bKZI8jLiv9kXrui6qLmX',
            'Content-Type': 'application/json'
        },
        body: JSON.stringify({
            amount: inputfloat,
            shop_id: 'WG76jKb7xl',
            order_id: `${CustomerSteamId}`,
            payer_pays_commission: '1',
            name: `Пополнение для ${CustomerSteamId}`,
        })
    })
        .then((res) => {
            if (res.ok) {
                return res.json();
            } else {
                console.log(res.status)
                return Promise.reject(`Ошибка: ${res.status}`);
            };
        })
        .then((res) => {
            window.open(res.link_page_url);
        })
}


function OvhUrlOverrite() {
    var slides = document.getElementsByClassName("nav-link");
    for (var i = 0; i < slides.length; i++) {
        var elelink = slides.item(i);
        var urlelelink = elelink.getAttribute("href");
        if (urlelelink.startsWith('https://pay.moscow.ovh')) {
            OvhPayUrl = urlelelink;
            console.log(OvhPayUrl);
            elelink.setAttribute("href", "javascript:;");
            elelink.setAttribute("onclick", "OpenOplata()");
        }
    }
}

function obtainShopSteamId() {
    if (CustomerSteamId != "0" && CustomerSteamId != "") {
        return;
    }
    var xmlHttp = new XMLHttpRequest();

    if (xmlHttp != null) {
        xmlHttp.open("GET", "/api/index.php?modules=users&action=getData", true);
        xmlHttp.send(null);
    }
    xmlHttp.onload = function (gjson) {
        var gjson = JSON.parse(xmlHttp.response);
        console.log(gjson);
        var preSteam = gjson.data.steamID;
        OvhPayUrl = "https://pay.moscow.ovh/?" + gjson.data.pay;
        if (preSteam > 76561100000000000 || !isNaN(preSteam)) {
            CustomerSteamId = preSteam.toString();
            OvhUrlOverrite();
        } else {
            console.log("error obtainShopSteamId! " + gjson);
        }
    }

}

function OpenOplata() {
    OpenPay('Oplata');
}

var DOMReady = function (a, b, c) { b = document, c = 'addEventListener'; b[c] ? b[c]('DOMContentLoaded', a) : window.attachEvent('onload', a) }
window.addEventListener("load", function () {
    try {
        obtainShopSteamId();
    } catch (e) {
        console.log('element not found ' + e);
    }
});
